class AddCreatedByToRepair < ActiveRecord::Migration
  def change
    add_column :repairs, :created_by, :integer
    add_column :repairs, :updated_by, :integer
  end
end
