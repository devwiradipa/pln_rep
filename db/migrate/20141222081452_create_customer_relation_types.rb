class CreateCustomerRelationTypes < ActiveRecord::Migration
  def change
    create_table :customer_relation_types do |t|
      t.string :name
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
