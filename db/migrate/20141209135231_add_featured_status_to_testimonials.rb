class AddFeaturedStatusToTestimonials < ActiveRecord::Migration
  def change
    add_column :testimonials, :featured, :integer
  end
end
