class AddAttachmentFilePksToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :file_pks
    end
  end

  def self.down
    drop_attached_file :users, :file_pks
  end
end
