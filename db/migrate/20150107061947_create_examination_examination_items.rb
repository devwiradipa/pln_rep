class CreateExaminationExaminationItems < ActiveRecord::Migration
  def change
    create_table :examination_examination_items do |t|
      t.integer :examination_id
      t.integer :examination_item_id
      t.integer :condition
      t.text :note

      t.timestamps
    end
  end
end
