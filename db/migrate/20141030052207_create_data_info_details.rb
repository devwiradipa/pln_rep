class CreateDataInfoDetails < ActiveRecord::Migration
  def change
    create_table :data_info_details do |t|
      t.string :name
      t.integer :created_by
      t.integer :updated_by
      t.integer :data_info_id

      t.timestamps
    end
  end
end
