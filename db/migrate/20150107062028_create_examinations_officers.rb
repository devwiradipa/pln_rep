class CreateExaminationsOfficers < ActiveRecord::Migration
  def change
    create_table :examinations_officers, :id => false, :force => true do |t|
      t.integer :examination_id
      t.integer :officer_id

      t.timestamps
    end
  end
end
