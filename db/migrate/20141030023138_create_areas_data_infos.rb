class CreateAreasDataInfos < ActiveRecord::Migration
  def change
    create_table :areas_data_infos, :id => false, :force => true do |t|
      t.integer :area_id
      t.integer :data_info_id

      t.timestamps
    end
  end
end
