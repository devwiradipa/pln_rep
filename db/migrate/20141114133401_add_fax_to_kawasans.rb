class AddFaxToKawasans < ActiveRecord::Migration
  def change
    add_column :kawasans, :alamat, :text
    add_column :kawasans, :telp, :string
    add_column :kawasans, :fax, :string
  end
end
