class AddUserReadToDataFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :user_read, :integer
  end
end
