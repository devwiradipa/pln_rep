class AddBebanMinToMonthlyInfos < ActiveRecord::Migration
  def change
    add_column :monthly_infos, :beban_min, :double
    add_column :monthly_infos, :beban_rata, :double
    add_column :monthly_infos, :beban_maks, :double
  end
end
