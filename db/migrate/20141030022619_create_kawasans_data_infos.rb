class CreateKawasansDataInfos < ActiveRecord::Migration
  def change
    create_table :kawasans_data_infos, :id => false, :force => true do |t|
      t.integer :kawasan_id
      t.integer :data_info_id

      t.timestamps
    end
  end
end
