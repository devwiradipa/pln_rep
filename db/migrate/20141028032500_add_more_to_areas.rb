class AddMoreToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :area_id, :string
    add_column :areas, :alamat, :text
    add_column :areas, :telp, :string
    add_column :areas, :lattitude, :float
    add_column :areas, :longitude, :float
    add_column :areas, :upi_code, :string
  end
end
