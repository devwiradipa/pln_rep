class AddReadByToDataFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :read_by, :integer
    add_column :data_feedbacks, :read_at, :datetime
  end
end
