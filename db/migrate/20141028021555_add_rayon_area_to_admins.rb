class AddRayonAreaToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :rayon_id, :integer
    add_column :admins, :area_id, :integer
    add_column :admins, :name, :string
  end
end
