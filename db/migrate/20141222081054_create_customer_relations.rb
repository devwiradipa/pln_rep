class CreateCustomerRelations < ActiveRecord::Migration
  def change
    create_table :customer_relations do |t|
      t.date :relation_date
      t.time :relation_time
      t.integer :user_id
      t.integer :customer_relation_type_id
      t.text :description

      t.timestamps
    end
  end
end
