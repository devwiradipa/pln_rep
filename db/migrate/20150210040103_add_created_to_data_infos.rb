class AddCreatedToDataInfos < ActiveRecord::Migration
  def change
    add_column :data_infos, :created_by, :integer
  end
end
