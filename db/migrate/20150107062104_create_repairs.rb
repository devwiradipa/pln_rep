class CreateRepairs < ActiveRecord::Migration
  def change
    create_table :repairs do |t|
      t.integer :user_id
      t.integer :examination_id

      t.timestamps
    end
  end
end
