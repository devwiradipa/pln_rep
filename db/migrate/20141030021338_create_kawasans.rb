class CreateKawasans < ActiveRecord::Migration
  def change
    create_table :kawasans do |t|
      t.string :kawasan_id
      t.string :nama_kawasan
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
