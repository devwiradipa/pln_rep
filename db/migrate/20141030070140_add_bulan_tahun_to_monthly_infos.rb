class AddBulanTahunToMonthlyInfos < ActiveRecord::Migration
  def change
    add_column :monthly_infos, :bulan_blth, :integer
    add_column :monthly_infos, :tahun_blth, :integer
  end
end
