class CreateExaminations < ActiveRecord::Migration
  def change
    create_table :examinations do |t|
      t.string :shift
      t.datetime :examination_time
      t.integer :examination_indicator_id
      t.integer :created_by
      t.integer :updated_by
      t.integer :user_id

      t.timestamps
    end
  end
end
