class CreateControllerModels < ActiveRecord::Migration
  def change
    create_table :controller_models do |t|
      t.string :category
      t.integer :created_by
      t.text :description
      t.string :name
      t.string :path
      t.string :position
      t.integer :updated_by
      t.string :controller_model_group
      t.integer :order_number

      t.timestamps
    end
  end
end
