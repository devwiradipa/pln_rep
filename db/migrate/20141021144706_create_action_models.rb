class CreateActionModels < ActiveRecord::Migration
  def change
    create_table :action_models do |t|
      t.integer :controller_model_id
      t.string :name
      t.text :descriptio
      t.string :path
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
