class AddFeaturedStatusToSlideshows < ActiveRecord::Migration
  def change
    add_column :slideshows, :featured_status, :integer
  end
end
