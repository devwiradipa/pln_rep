class AddMoreToUsers < ActiveRecord::Migration
  def change
    add_column :users, :unitap, :string
    add_column :users, :unitup, :string
    add_column :users, :kode_jenis_mutasi, :string
    add_column :users, :bulan_tahun_mutasi, :string
    add_column :users, :penunjukan_alamat, :string
    add_column :users, :nomor_bangunan, :string
    add_column :users, :keterangan_bangunan, :string
    add_column :users, :nomor_rt, :string
    add_column :users, :nomor_rw, :string
    add_column :users, :nomor_dlm_rt, :string
    add_column :users, :trafo_arus_primer_kwh, :integer
    add_column :users, :trafo_arus_sekunder_kwh, :integer
    add_column :users, :trafo_tegangan_primer_kwh, :integer
    add_column :users, :trafo_tegangan_sekunder_kwh, :integer
    add_column :users, :kapasitas_trafo, :integer
    add_column :users, :jenis_tipe_gardu, :string
    add_column :users, :tegangan, :integer
    add_column :users, :rupiah_ujl, "bigint"
    add_column :users, :no_kuit_ujl, :string
    add_column :users, :tgl_kuit_ujl_date, :date
    add_column :users, :koordinat_x, :float
    add_column :users, :koordinat_y, :float
  end
end
