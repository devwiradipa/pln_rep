class CreateDataFeedbacks < ActiveRecord::Migration
  def change
    create_table :data_feedbacks do |t|
      t.string :idpel
      t.text :feedback_plg
      t.date :tgl_feedback
      t.integer :parent_id
      t.integer :user_id

      t.timestamps
    end
  end
end
