class AddStatusFeedbackToDataFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :status_feedback, :string
  end
end
