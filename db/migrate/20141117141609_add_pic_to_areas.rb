class AddPicToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :pic_name_1, :string
    add_column :areas, :pic_jabatan_1, :string
    add_column :areas, :pic_hp_1, :string
    add_column :areas, :pic_name_2, :string
    add_column :areas, :pic_jabatan_2, :string
    add_column :areas, :pic_hp_2, :string
    add_column :areas, :pic_name_3, :string
    add_column :areas, :pic_jabatan_3, :string
    add_column :areas, :pic_hp_3, :string
    add_column :areas, :pic_name_4, :string
    add_column :areas, :pic_jabatan_4, :string
    add_column :areas, :pic_hp_4, :string

    add_column :rayons, :pic_name_1, :string
    add_column :rayons, :pic_jabatan_1, :string
    add_column :rayons, :pic_hp_1, :string
    add_column :rayons, :pic_name_2, :string
    add_column :rayons, :pic_jabatan_2, :string
    add_column :rayons, :pic_hp_2, :string
    add_column :rayons, :pic_name_3, :string
    add_column :rayons, :pic_jabatan_3, :string
    add_column :rayons, :pic_hp_3, :string
    add_column :rayons, :pic_name_4, :string
    add_column :rayons, :pic_jabatan_4, :string
    add_column :rayons, :pic_hp_4, :string

    add_column :kawasans, :pic_name_1, :string
    add_column :kawasans, :pic_jabatan_1, :string
    add_column :kawasans, :pic_hp_1, :string
    add_column :kawasans, :pic_name_2, :string
    add_column :kawasans, :pic_jabatan_2, :string
    add_column :kawasans, :pic_hp_2, :string
    add_column :kawasans, :pic_name_3, :string
    add_column :kawasans, :pic_jabatan_3, :string
    add_column :kawasans, :pic_hp_3, :string
    add_column :kawasans, :pic_name_4, :string
    add_column :kawasans, :pic_jabatan_4, :string
    add_column :kawasans, :pic_hp_4, :string
  end
end
