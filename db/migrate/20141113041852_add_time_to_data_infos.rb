class AddTimeToDataInfos < ActiveRecord::Migration
  def change
    add_column :data_infos, :time_start, :time
    add_column :data_infos, :time_end, :time
    add_column :data_infos, :duration, :float
  end
end
