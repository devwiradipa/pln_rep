class AddClosedTimeToDataFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :closed_time, :timestamp
  end
end
