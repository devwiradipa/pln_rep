class CreateDayas < ActiveRecord::Migration
  def change
    create_table :dayas do |t|
      t.column :name, "bigint"
      t.text :description

      t.timestamps
    end
  end
end
