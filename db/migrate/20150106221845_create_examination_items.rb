class CreateExaminationItems < ActiveRecord::Migration
  def change
    create_table :examination_items do |t|
      t.string :name
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
