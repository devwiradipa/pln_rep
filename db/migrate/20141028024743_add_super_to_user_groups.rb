class AddSuperToUserGroups < ActiveRecord::Migration
  def change
    add_column :user_groups, :super, :integer
  end
end
