class CreateMonthlyInfos < ActiveRecord::Migration
  def change
    create_table :monthly_infos do |t|
      t.column :user_id, :bigint
      t.string :idpel
      t.date :blth
      t.column :kwh_lwbp, :double
      t.column :kwh_wbp, :double
      t.column :kwh_kvarh, :double
      t.column :kwh_total, :double
      t.column :rp_lwbp, :bigint
      t.column :rp_wbp, :bigint
      t.column :rp_kvarh, :bigint
      t.column :rptag, :bigint
      t.column :kvamaks, :double
      t.float :durasi_gangguan
      t.column :frekuensi_gangguan, :bigint

      t.timestamps
    end
  end
end
