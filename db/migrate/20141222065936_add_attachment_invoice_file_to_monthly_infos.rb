class AddAttachmentInvoiceFileToMonthlyInfos < ActiveRecord::Migration
  def self.up
    change_table :monthly_infos do |t|
      t.attachment :invoice_file
    end
  end

  def self.down
    drop_attached_file :monthly_infos, :invoice_file
  end
end
