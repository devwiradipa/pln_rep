class AddFeedbackTypeToFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :feedback_type_id, :integer
  end
end
