class AddManyManyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :no_telepon_contact, :string
    add_column :users, :no_telp_contact_2, :string
    add_column :users, :no_hp_contact_2, :string
    add_column :users, :no_fax_contact, :string
  end
end
