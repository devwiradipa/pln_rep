class AddFaxToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :fax, :string
  end
end
