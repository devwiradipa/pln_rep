class AddUserGroupToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :user_group_id, :integer
  end
end
