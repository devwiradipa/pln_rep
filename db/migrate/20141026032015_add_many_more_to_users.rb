class AddManyMoreToUsers < ActiveRecord::Migration
  def change
    add_column :users, :lingkungan, :string
    add_column :users, :desa_kelurahan, :string
    add_column :users, :subdistrict_id, :integer
    add_column :users, :district_id, :integer
    add_column :users, :province_id, :integer
    add_column :users, :jenis_layanan_id, :integer
    add_column :users, :nomor_pks, :string
    add_column :users, :tanggal_pks, :date
    add_column :users, :merk_meter_id, :integer
    add_column :users, :type_meter_id, :integer
    add_column :users, :nomor_meter, :string
    add_column :users, :kosntanta_meter, :integer
    add_column :users, :ct, :string
    add_column :users, :pt, :string
    add_column :users, :faktor_kali_meter, :string
    add_column :users, :tanggal_pasang_baru, :date
    add_column :users, :tanggal_layanan_premium, :date
    add_column :users, :pasokan_gi_1, :string
    add_column :users, :pasokan_penyulang_1, :string
    add_column :users, :pasokan_gi_2, :string
    add_column :users, :pasokan_penyulang_2, :string
    add_column :users, :pasokan_gi_3, :string
    add_column :users, :pasokan_penyulang_3, :string
    add_column :users, :pasokan_gi_4, :string
    add_column :users, :pasokan_penyulang_4, :string
    add_column :users, :nomor_klui, :string
    add_column :users, :jenis_bahan_baku, :string
    add_column :users, :jenis_komoditi_produk, :string
    add_column :users, :kapasitas_produksi_per_tahun, :string
    add_column :users, :jumlah_tenaga_kerja, :integer
    add_column :users, :mesin_peralatan_utama, :string
    add_column :users, :mesin_peralatan_pembantu, :string
    add_column :users, :nama_dirut_string, :string
    add_column :users, :jabatan_contact, :string
  end
end
