class CreateExaminationIndicatorsExaminationItems < ActiveRecord::Migration
  def change
    create_table :examination_indicators_examination_items, :id => false, :force => true do |t|
      t.integer :examination_item_id
      t.integer :examination_indicator_id

      t.timestamps
    end
  end
end
