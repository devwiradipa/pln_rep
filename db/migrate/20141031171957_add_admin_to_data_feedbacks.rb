class AddAdminToDataFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :admin_id, :integer
  end
end
