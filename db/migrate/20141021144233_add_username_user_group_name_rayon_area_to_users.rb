class AddUsernameUserGroupNameRayonAreaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :user_group_id, :integer
    add_column :users, :name, :string
    add_column :users, :rayon_id, :integer
    add_column :users, :area_id, :integer
  end
end
