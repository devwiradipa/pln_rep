class CreateActionModelsUserGroups < ActiveRecord::Migration
  def change
    create_table :action_models_user_groups, :id => false, :force => true do |t|
      t.integer :action_model_id
      t.integer :user_group_id

      t.timestamps
    end
  end
end
