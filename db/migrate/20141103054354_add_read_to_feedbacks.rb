class AddReadToFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :read, :string
  end
end
