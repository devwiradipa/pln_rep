class AddListEmailToDataInfos < ActiveRecord::Migration
  def change
    add_column :data_infos, :list_email, :text
  end
end
