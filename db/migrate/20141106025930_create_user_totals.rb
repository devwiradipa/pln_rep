class CreateUserTotals < ActiveRecord::Migration
  def change
    create_table :user_totals do |t|
      t.integer :bulan_blth
      t.integer :tahun_blth
      t.integer :total

      t.timestamps
    end
  end
end
