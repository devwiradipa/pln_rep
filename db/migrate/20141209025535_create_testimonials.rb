class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.string :body
      t.string :testimonial_by
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
