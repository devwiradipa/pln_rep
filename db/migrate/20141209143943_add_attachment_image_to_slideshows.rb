class AddAttachmentImageToSlideshows < ActiveRecord::Migration
  def self.up
    change_table :slideshows do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :slideshows, :image
  end
end
