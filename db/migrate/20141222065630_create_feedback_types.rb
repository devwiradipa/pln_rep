class CreateFeedbackTypes < ActiveRecord::Migration
  def change
    create_table :feedback_types do |t|
      t.string :name
      t.integer :created_by
      t.integer :updated_by
      t.integer :type_status

      t.timestamps
    end
  end
end
