class CreateJenisLayanans < ActiveRecord::Migration
  def change
    create_table :jenis_layanans do |t|
      t.string :name
      t.integer :tarif
      t.integer :created_by
      t.integer :updated_by
      t.text :description

      t.timestamps
    end
  end
end
