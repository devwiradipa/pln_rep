class AddAttachmentFileToMonthlyInfoFiles < ActiveRecord::Migration
  def self.up
    change_table :monthly_info_files do |t|
      t.attachment :file
    end
  end

  def self.down
    drop_attached_file :monthly_info_files, :file
  end
end
