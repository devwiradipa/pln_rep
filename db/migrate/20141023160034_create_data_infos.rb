class CreateDataInfos < ActiveRecord::Migration
  def change
    create_table :data_infos do |t|
      t.string :idpel
      t.text :info_gangguan
      t.date :tgl_info_gangguan
      t.text :info_ren_pemeliharaan
      t.date :tgl_ren_pemeliharaan
      t.text :info_dari_pln
      t.date :tgl_dari_pln
      t.text :info_lainnya
      t.date :tgl_info_lainnya

      t.timestamps
    end
  end
end
