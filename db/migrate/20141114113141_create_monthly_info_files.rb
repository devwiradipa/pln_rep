class CreateMonthlyInfoFiles < ActiveRecord::Migration
  def change
    create_table :monthly_info_files do |t|
      t.string :name

      t.timestamps
    end
  end
end
