class CreateExaminationIndicators < ActiveRecord::Migration
  def change
    create_table :examination_indicators do |t|
      t.string :name
      t.integer :status
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
