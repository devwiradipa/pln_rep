class AddManyToMany < ActiveRecord::Migration
  def change
  	add_column :admins, :rayon_new_id, :string
    add_column :admins, :area_new_id, :string
    add_column :users, :rayon_new_id, :string
    add_column :users, :area_new_id, :string
  end
end
