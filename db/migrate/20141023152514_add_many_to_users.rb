class AddManyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :idpel, :string
    add_column :users, :tarip, :string
    add_column :users, :daya, :integer
    add_column :users, :kdaya, :string
    add_column :users, :pasokan_gi, :string
    add_column :users, :pasokan_penyulang, :string
    add_column :users, :nomor_gardu, :string
    add_column :users, :nama_gardu, :string
    add_column :users, :jurusan_tiang, :string
    add_column :users, :alamat_pelanggan, :text
    add_column :users, :kode_pos, :string
    add_column :users, :nomor_telepon, :string
    add_column :users, :nomor_fax, :string
    add_column :users, :email_pelanggan, :string
    add_column :users, :email_kantor, :string
    add_column :users, :nama_contact, :string
    add_column :users, :nomor_hp_contact, :string
    add_column :users, :email_contact, :string
    add_column :users, :perkiraan_biaya_investasi, "bigint"
    add_column :users, :keterangan_premium, :string
    add_column :users, :status_premium, :string
    add_column :users, :jenis_usaha, :string
    add_column :users, :status_publish, :string
  end
end
