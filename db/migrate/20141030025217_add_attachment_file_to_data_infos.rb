class AddAttachmentFileToDataInfos < ActiveRecord::Migration
  def self.up
    change_table :data_infos do |t|
      t.attachment :file
    end
  end

  def self.down
    drop_attached_file :data_infos, :file
  end
end
