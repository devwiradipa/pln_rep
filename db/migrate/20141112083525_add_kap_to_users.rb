class AddKapToUsers < ActiveRecord::Migration
  def change
    add_column :users, :kapasitas_trafo_induk_1, :string
    add_column :users, :nomor_trafo_induk_1, :string
    add_column :users, :kapasitas_trafo_induk_2, :string
    add_column :users, :nomor_trafo_induk_2, :string
    add_column :users, :kapasitas_trafo_induk_3, :string
    add_column :users, :nomor_trafo_induk_3, :string
    add_column :users, :kapasitas_trafo_induk_4, :string
    add_column :users, :nomor_trafo_induk_4, :string
  end
end
