class AddAgainToUsers < ActiveRecord::Migration
  def change
    add_column :users, :nama_contact_2, :string
    add_column :users, :jabatan_contact_2, :string
    add_column :users, :email_contact_2, :string
    add_column :users, :nama_wakil_dirut, :string
  end
end
