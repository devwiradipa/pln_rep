class AddUserReadByToDataFeedbacks < ActiveRecord::Migration
  def change
    add_column :data_feedbacks, :user_read_by, :integer
    add_column :data_feedbacks, :user_read_at, :datetime
  end
end
