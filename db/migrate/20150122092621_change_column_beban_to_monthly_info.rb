class ChangeColumnBebanToMonthlyInfo < ActiveRecord::Migration
  def up
    change_column :monthly_infos, :beban_min, :double
    change_column :monthly_infos, :beban_rata, :double
  end

  def down
    change_column :monthly_infos, :beban_min, :bigint
    change_column :monthly_infos, :beban_rata, :bigint
  end
end
