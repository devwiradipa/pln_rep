class AddMoreToRayons < ActiveRecord::Migration
  def change
    add_column :rayons, :rayon_id, :string
    add_column :rayons, :alamat, :text
    add_column :rayons, :telp, :string
    add_column :rayons, :fax, :string
    add_column :rayons, :area_new_id, :string
    add_column :rayons, :lattitude, :float
    add_column :rayons, :longitude, :float
  end
end
