class AddCreatedUpdatedToCustomerRelations < ActiveRecord::Migration
  def change
    add_column :customer_relations, :created_by, :integer
    add_column :customer_relations, :updated_by, :integer
  end
end
