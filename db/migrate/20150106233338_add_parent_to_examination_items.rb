class AddParentToExaminationItems < ActiveRecord::Migration
  def change
    add_column :examination_items, :parent_id, :integer
  end
end
