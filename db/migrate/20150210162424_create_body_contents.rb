class CreateBodyContents < ActiveRecord::Migration
  def change
    create_table :body_contents do |t|
      t.string :title
      t.text :body
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
