class CreateRepairsExaminationItems < ActiveRecord::Migration
  def change
    create_table :repairs_examination_items do |t|
      t.integer :repair_id
      t.integer :examination_item_id
      t.datetime :repair_time
      t.integer :officer_id
      t.text :note

      t.timestamps
    end
  end
end
