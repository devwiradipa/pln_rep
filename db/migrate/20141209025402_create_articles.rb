class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :body
      t.integer :created_by
      t.integer :updated_by
      t.integer :featured_status
      t.integer :publish_status

      t.timestamps
    end
  end
end
