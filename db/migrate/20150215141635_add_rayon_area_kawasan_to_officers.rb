class AddRayonAreaKawasanToOfficers < ActiveRecord::Migration
  def change
    add_column :officers, :kawasan_id, :integer
    add_column :officers, :area_id, :string
    add_column :officers, :rayon_id, :string
  end
end
