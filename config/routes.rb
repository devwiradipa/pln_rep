Rails.application.routes.draw do

  post "json/admin/rayons/get_all" => "admin/rayons#get_all" 
  post "json/get_district" => "json#get_district" 
  post "json/get_subdistrict" => "json#get_subdistrict" 
  post "json/admin/data_info_details/get_all" => "admin/data_info_details#get_all" 
  post "json/admin/monthly_info_summaries/get_data" => "admin/monthly_info_summaries#get_data" 
  post "json/homes/get_data" => "homes#get_data" 

  devise_for :admins

  mount Ckeditor::Engine => '/ckeditor'

  devise_for :users

  resources :mains
  resources :homes
  resources :histories
  resources :data_feedbacks
  resources :data_infos
  resources :articles
  resources :testimonials
  resources :admins do
    collection do
      get :edit_password
    end

    member do
      put :update_password
    end        
  end
  
  resources :my_accounts
  resources :contacts
  resources :user_totals do
    member do
      get :execute
    end
  end
  resources :users do
    collection do
      get :edit_password
    end

    member do
      put :update_password
    end        
  end

  namespace :admin do
    resources :admins
    resources :users do 
      member do
        get :reset_password
        get :send_to_trash
        get :restore
      end

      collection do
        get :trash
      end
    end
    resources :premium_users do 
      member do
        get :reset_password
        get :send_to_trash
        get :restore
      end
      
      collection do
        get :trash
      end
    end
    resources :user_groups do 
      member do
        get :update_status
      end
    end
    resources :controller_models
    resources :action_models
    resources :areas
    resources :articles do
      member do
        get :featured
        get :publish
      end
    end
    resources :testimonials do
      member do
        get :featured
      end
    end
    resources :slideshows do
      member do
        get :featured
      end
    end
    resources :rayons
    resources :data_infos
    resources :data_info_details
    resources :monthly_infos do 
      collection do
        get :upload_invoice_form
        put :upload_invoice
      end
    end
    resources :monthly_info_files
    resources :monthly_info_summaries do
      collection do
        get "export/:id" => "user_levels#export", as: "export"
      end
    end
    resources :jenis_layanans
    resources :jenis_infos
    resources :merk_meters
    resources :type_meters
    resources :data_feedbacks do
      member do 
        get :close
      end
    end
    resources :my_accounts
    resources :kawasans
    resources :master_tarifs
    resources :dayas
    resources :feedback_types
    resources :customer_relations
    resources :customer_relation_types
    resources :officers
    resources :examination_items
    resources :repairs
    resources :examinations
    resources :examination_indicators do
      member do
        get :active
      end
    end
    resources :examinations
    resources :repairs_examination_items
    resources :whats
  end

  get 'admins' => 'admins#index', :as => 'admin_root'
  
  root :to => "mains#index"  
end
