class UserGroup < ActiveRecord::Base
  has_many :action_models_user_groups, :dependent => :destroy
  has_many :action_models, :through => :action_models_user_groups

  has_many :admins, :dependent => :restrict_with_exception
end
