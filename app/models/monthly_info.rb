class MonthlyInfo < ActiveRecord::Base
  belongs_to :user
  belongs_to :monthly_info_file

  has_attached_file :invoice_file,
  :url => '/files/monthly_infos/:basename_:style_:id.:extension',
  :path => ":rails_root/public/files/monthly_infos/:basename_:style_:id.:extension"

  do_not_validate_attachment_file_type :invoice_file
  
  process_in_background :invoice_file
end
