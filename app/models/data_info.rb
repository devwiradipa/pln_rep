class DataInfo < ActiveRecord::Base
  belongs_to :jenis_info
  belongs_to :data_info_detail

  has_many :kawasans_data_infos
  has_many :kawasans, :through => :kawasans_data_infos

  has_many :areas_data_infos
  has_many :areas, :through => :areas_data_infos

  has_many :users_data_infos
  has_many :users, :through => :users_data_infos

  belongs_to :created, :class_name => "Admin", :foreign_key => "created_by"

  has_attached_file :file,
  :url => '/files/data_infos/:basename_:style_:id.:extension',
  :path => ":rails_root/public/files/data_infos/:basename_:style_:id.:extension"

  do_not_validate_attachment_file_type :file
end
