class CustomerRelation < ActiveRecord::Base
  belongs_to :user
  belongs_to :customer_relation_type
  belongs_to :created, :class_name => "Admin", :foreign_key => "created_by"
end
