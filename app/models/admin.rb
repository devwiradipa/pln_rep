class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable

  # paperclip
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/assets/missing.png",
    :url => '/images/admins/:basename_:style_:id.:extension',
    :path => ":rails_root/public/images/admins/:basename_:style_:id.:extension"

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  belongs_to :user_group
  belongs_to :rayon, :foreign_key => "rayon_new_id", :primary_key => "rayon_id"
  belongs_to :area, :foreign_key => "area_new_id", :primary_key => "area_id"
  belongs_to :kawasan
end
