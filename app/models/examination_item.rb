class ExaminationItem < ActiveRecord::Base
  belongs_to :parent, :class_name => "ExaminationItem"
  has_many :children, :class_name => "ExaminationItem", :foreign_key => "parent_id"

  has_many :examination_indicators_examination_items
  has_many :examination_indicators, :through => :examination_indicators_examination_items
  has_many :examination_examination_items, :dependent => :destroy
  has_many :examinations, :through => :examination_examination_items
  has_many :repairs_examination_items, :dependent => :destroy
end
