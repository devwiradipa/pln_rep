class Repair < ActiveRecord::Base
  belongs_to :user
  belongs_to :examination
  
  has_many :repairs_examination_items, :dependent => :destroy
  
  accepts_nested_attributes_for :repairs_examination_items, allow_destroy: true
end
