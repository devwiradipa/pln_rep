class MonthlyInfoFile < ActiveRecord::Base
  attr_accessor :file
  
  has_many :monthly_infos
  
  has_attached_file :file,
  :url => '/public/excels/monthly_info_files/:basename_:style_:id.:extension',
  :path => ":rails_root/public/excels/monthly_info_files/:basename_:style_:id.:extension"

  do_not_validate_attachment_file_type :file
  
  accepts_nested_attributes_for :monthly_infos, allow_destroy: true
  
  def monthly_info_processing(excel_instance, id)
    2.upto(excel_instance.last_row) do |line|
    	user = User.find(:first, :conditions => {:idpel => excel_instance.cell(line, 'A').to_i.to_s})

    	if user
    		monthly_info = MonthlyInfo.find(:first, :conditions => ["user_id = ? AND bulan_blth = ? AND tahun_blth = ?", user.id, excel_instance.cell(line, 'B').to_i, excel_instance.cell(line, 'C').to_i])
    	end

    	if monthly_info
			if monthly_info.user
	          if monthly_info.user.area
	            @areas_data_infos = AreasDataInfo.find(:all, :conditions => {:area_id => monthly_info.user.area.id})
	          else
	            @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")
	          end

	          @kawasans_data_infos = KawasansDataInfo.find(:all, :conditions => {:kawasan_id => monthly_info.user.kawasan_id})

	          @users_data_infos = UsersDataInfo.find(:all, :conditions => {:user_id => monthly_info.user_id})
	        else
	          @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")

	          @kawasans_data_infos = KawasansDataInfo.where("data_info_id IS NULL")

	          @users_data_infos = UsersDataInfo.where("data_info_id IS NULL")
	        end

    	@data_infos = DataInfo.find(:all, :conditions => ["jenis_info_id = 1 AND MONTH(tgl_info_gangguan) = ? AND YEAR(tgl_info_gangguan) = ? AND (id IN (?) OR id IN (?) OR id IN (?))", monthly_info.bulan_blth, monthly_info.tahun_blth, @areas_data_infos.map(&:data_info_id), @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id)])


    	beban_min = 0
    	beban_rata = 0
    	beban_maks = 0

    		if excel_instance.cell(line, 'E') && excel_instance.cell(line, 'F')
				kwh_total = excel_instance.cell(line, 'E').to_i+excel_instance.cell(line, 'F').to_i
			else
				kwh_total = 0
	        end

	        if excel_instance.cell(line, 'H') && excel_instance.cell(line, 'I') && excel_instance.cell(line, 'J')
	        	rptag = excel_instance.cell(line, 'H').to_i+excel_instance.cell(line, 'I').to_i+excel_instance.cell(line, 'J').to_i
	        else
	        	rptag = 0
	        end
	        
	        if kwh_total && kwh_total != 0 && excel_instance.cell(line, 'G') && excel_instance.cell(line, 'G') != 0
	        	beban_min = kwh_total / Math.sqrt(kwh_total*kwh_total+excel_instance.cell(line, 'G').to_i*excel_instance.cell(line, 'G').to_i)
	        else
	        	beban_min = 0
	        end

	        if user && excel_instance.cell(line, 'D') && user.faktor_kali_meter
	          beban_maks = excel_instance.cell(line, 'D').to_i * user.faktor_kali_meter.to_i
	        else
	          beban_maks = 0
	        end

	        if kwh_total
	        	beban_rata = kwh_total / 720.to_f
	        else
	        	beban_rata = 0
	        end
	      
	        monthly_info.update_attributes(:durasi_gangguan => @data_infos.sum(&:duration).to_f, 
	        	:frekuensi_gangguan => @data_infos.count,
	        	:kvamaks => excel_instance.cell(line, 'D'), 
	        	:kwh_lwbp => excel_instance.cell(line, 'E'),
			      :kwh_wbp => excel_instance.cell(line, 'F'),
			      :kwh_total => kwh_total,
			      :kwh_kvarh => excel_instance.cell(line, 'G'),
			      :rp_lwbp => excel_instance.cell(line, 'H'),
			      :rp_wbp => excel_instance.cell(line, 'I'),
			      :rp_kvarh => excel_instance.cell(line, 'J'),
			      :rptag => rptag,
			      :beban_maks => beban_maks,
			      :beban_min => beban_min,
			      :beban_rata => beban_rata,
			      :monthly_info_file_id => id)
    	else

		    monthly_info = MonthlyInfo.new
		    user = User.find(:first, :conditions => {:idpel => excel_instance.cell(line, 'A').to_i.to_s})
		    if user
		    	monthly_info.user_id = user.id
		    end

	      monthly_info.bulan_blth = excel_instance.cell(line, 'B')
	      monthly_info.tahun_blth = excel_instance.cell(line, 'C')
	      monthly_info.kvamaks = excel_instance.cell(line, 'D')
	      monthly_info.kwh_lwbp = excel_instance.cell(line, 'E')
	      monthly_info.kwh_wbp = excel_instance.cell(line, 'F')
	      monthly_info.kwh_kvarh = excel_instance.cell(line, 'G')
	      monthly_info.rp_lwbp = excel_instance.cell(line, 'H')
	      monthly_info.rp_wbp = excel_instance.cell(line, 'I')
	      monthly_info.rp_kvarh = excel_instance.cell(line, 'J')
	      monthly_info.monthly_info_file_id = id

	      if monthly_info.kwh_lwbp && monthly_info.kwh_wbp
	      	monthly_info.kwh_total = monthly_info.kwh_lwbp+monthly_info.kwh_wbp
	      else
	      	monthly_info.kwh_total = 0
	      end

	      if monthly_info.rp_lwbp && monthly_info.rp_wbp && monthly_info.rp_kvarh
	      	monthly_info.rptag = monthly_info.rp_lwbp+monthly_info.rp_wbp+monthly_info.rp_kvarh
	      else
	      	monthly_info.rptag = 0
	      end
	      
	      if monthly_info.kwh_total && monthly_info.kwh_kvarh && monthly_info.kwh_kvarh != 0 && monthly_info.kwh_total != 0
	      	monthly_info.beban_min = monthly_info.kwh_total / Math.sqrt(monthly_info.kwh_total*monthly_info.kwh_total+monthly_info.kwh_kvarh*monthly_info.kwh_kvarh)
	      else
	      	monthly_info.beban_min = 0
	      end

	      if user && monthly_info.kvamaks && user.faktor_kali_meter
	        monthly_info.beban_maks = monthly_info.kvamaks * user.faktor_kali_meter.to_i
	      else
	      	monthly_info.beban_maks = 0
	      end

	      if monthly_info.kwh_total
	      	monthly_info.beban_rata = monthly_info.kwh_total / 720.to_f
	      else
	      	monthly_info.beban_rata = 0
	      end

	      monthly_info.save

	      	if monthly_info.user
	          if monthly_info.user.area
	            @areas_data_infos = AreasDataInfo.find(:all, :conditions => {:area_id => monthly_info.user.area.id})
	          else
	            @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")
	          end

	          @kawasans_data_infos = KawasansDataInfo.find(:all, :conditions => {:kawasan_id => monthly_info.user.kawasan_id})

	          @users_data_infos = UsersDataInfo.find(:all, :conditions => {:user_id => monthly_info.user_id})
	        else
	          @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")

	          @kawasans_data_infos = KawasansDataInfo.where("data_info_id IS NULL")

	          @users_data_infos = UsersDataInfo.where("data_info_id IS NULL")
	        end

		    @data_infos = DataInfo.find(:all, :conditions => ["jenis_info_id = 1 AND MONTH(tgl_info_gangguan) = ? AND YEAR(tgl_info_gangguan) = ? AND (id IN (?) OR id IN (?) OR id IN (?))", monthly_info.bulan_blth, monthly_info.tahun_blth, @areas_data_infos.map(&:data_info_id), @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id)])
	        monthly_info.update_attributes(:durasi_gangguan => @data_infos.sum(&:duration).to_f, :frekuensi_gangguan => @data_infos.count )
	    end
    end
  end
end
