class Article < ActiveRecord::Base
  belongs_to :created, :class_name => "Admin", :foreign_key => "created_by"

  has_attached_file :image,
       :styles => {
       :side => "165x165#",
       :small => "234x131#",
       :large => "780x300#"    },
  :url => '/images/articles/:basename_:style_:id.:extension',
  :path => ":rails_root/public/images/articles/:basename_:style_:id.:extension"

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
