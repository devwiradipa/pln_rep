class ExaminationIndicatorsExaminationItem < ActiveRecord::Base
  belongs_to :examination_indicator
  belongs_to :examination_item
end
