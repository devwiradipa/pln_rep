class Area < ActiveRecord::Base
  has_many :users

  has_many :rayons, :foreign_key => "area_new_id", :primary_key => "area_id"
  has_many :users, :foreign_key => "area_new_id", :primary_key => "area_id"
  has_many :admins, :foreign_key => "area_new_id", :primary_key => "area_id"
  has_many :kawasans

  has_many :areas_data_infos
  has_many :data_infos, :through => :areas_data_infos
end
