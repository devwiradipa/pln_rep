class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :timeoutable
  # attr_accessible :title, :body

  has_attached_file :file_pks,
  :url => '/files/spjbtl/:basename_:style_:id.:extension',
  :path => ":rails_root/public/files/spjbtl/:basename_:style_:id.:extension"

  # paperclip
  has_attached_file :avatar, :styles => { :medium => "300x300#", :thumb => "150x150#" }, :default_url => "/assets/missing.png",
    :url => '/images/users/:basename_:style_:id.:extension',
    :path => ":rails_root/public/images/users/:basename_:style_:id.:extension"

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  do_not_validate_attachment_file_type :file_pks

  validates :idpel, :uniqueness => true
  validates :username, :uniqueness => true

  belongs_to :user_group
  belongs_to :area, :foreign_key => "area_new_id", :primary_key => "area_id"
  belongs_to :rayon, :foreign_key => "rayon_new_id", :primary_key => "rayon_id"
  belongs_to :jenis_layanan
  belongs_to :province
  belongs_to :district
  belongs_to :kawasan
  belongs_to :subdistrict
  belongs_to :merk_meter
  belongs_to :type_meter
  belongs_to :master_daya, :class_name => "Daya", :foreign_key => "daya_id"
  belongs_to :master_tarif

  has_many :users_data_infos
  has_many :data_infos, :through => :users_data_infos

  has_many :monthly_infos
  has_many :repairs
  has_many :examinations

  def active_for_authentication?
    #remember to call the super
    #then put our own check to determine "active" state using 
    #our own "is_active" column
    super and self.jenis_layanan and self.jenis_layanan.tipe_jenis_layanan_id?
  end
end
