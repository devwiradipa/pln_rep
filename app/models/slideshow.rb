class Slideshow < ActiveRecord::Base
  has_attached_file :image,
       :styles => {
       :side => "237x178#",
       :small => "234x131#",
       :slider => "1920x550#"},
	  :url => '/images/slideshows/:basename_:style_:id.:extension',
	  :path => ":rails_root/public/images/slideshows/:basename_:style_:id.:extension"

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
