class DataFeedback < ActiveRecord::Base
  belongs_to :parent, :class_name => "DataFeedback"
  has_many :replies, :class_name => "DataFeedback", :foreign_key => "parent_id"
  belongs_to :feedback_type

  belongs_to :user
  belongs_to :admin
end
