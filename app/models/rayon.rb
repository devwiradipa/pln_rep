class Rayon < ActiveRecord::Base
  has_many :users, :foreign_key => "rayon_new_id", :primary_key => "rayon_id"
  has_many :admins, :foreign_key => "rayon_new_id", :primary_key => "rayon_id"
  belongs_to :area, :foreign_key => "area_new_id", :primary_key => "area_id"
end
