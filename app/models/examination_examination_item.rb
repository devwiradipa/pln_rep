class ExaminationExaminationItem < ActiveRecord::Base
  # attr_accessible :condition, :examination_id, :examination_item_id, :note
  
  belongs_to :examination
  belongs_to :examination_item
end
