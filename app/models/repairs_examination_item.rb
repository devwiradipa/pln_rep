class RepairsExaminationItem < ActiveRecord::Base
  belongs_to :repair
  belongs_to :examination_item
  belongs_to :officer
end
