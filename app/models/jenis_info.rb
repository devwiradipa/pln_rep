class JenisInfo < ActiveRecord::Base
  has_many :data_infos

  has_many :data_info_details
end
