class Officer < ActiveRecord::Base
  has_many :examinations_officers
  has_many :repairs_examination_items
  has_many :examinations, :through => :examinations_officers 

  belongs_to :kawasan
  belongs_to :rayon, :primary_key => "rayon_id"
  belongs_to :area, :primary_key => "area_id"
end
