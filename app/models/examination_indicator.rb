class ExaminationIndicator < ActiveRecord::Base
  has_many :examinations
  has_many :examination_indicators_examination_items, :dependent => :destroy
  has_many :examination_items, :through => :examination_indicators_examination_items
end
