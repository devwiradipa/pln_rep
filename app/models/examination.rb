class Examination < ActiveRecord::Base
  has_one :repair, :dependent => :destroy
  has_many :examination_examination_items, :dependent => :destroy
  has_many :examination_items, :through => :examination_examination_items
  has_many :examinations_officers
  has_many :officers, :through => :examinations_officers 
  
  belongs_to :user
  belongs_to :examination_indicator
  
  accepts_nested_attributes_for :examination_examination_items, allow_destroy: true
end
