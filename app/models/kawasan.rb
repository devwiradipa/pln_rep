class Kawasan < ActiveRecord::Base
  has_many :kawasans_data_infos
  has_many :data_infos, :through => :kawasans_data_infos
  has_many :users
  has_many :admins

  belongs_to :area
end
