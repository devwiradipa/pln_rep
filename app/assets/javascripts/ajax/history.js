$(function () {
	var blth_tahun_awal = $("#blth_tahun_awal").val();
	var blth_bulan_awal = $("#blth_bulan_awal").val();
	var blth_tahun_akhir = $("#blth_tahun_akhir").val();
	var blth_bulan_akhir = $("#blth_bulan_akhir").val();
	var user_id = $("#user_id").val();

	$.ajax({
		type: "POST",
		data: "blth_bulan_awal="+blth_bulan_awal+"&blth_tahun_awal="+blth_tahun_awal+"&blth_bulan_akhir="+blth_bulan_akhir+"&blth_tahun_akhir="+blth_tahun_akhir+"&user_id="+user_id,
		url: "/json/homes/get_data",
		success: function(response){
			var data = response;

			$('#grafik').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Grafik Gangguan Listrik'
				},
				xAxis: {
					categories: data.blth
				},
				yAxis: {
					title: {
						text: 'Kali/Jam'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				series: [{
					name: 'Durasi Gangguan',
					data: data.durasi_gangguan
				},{
					name: 'Frekuensi Gangguan',
					data: data.frekuensi_gangguan
				}]
			});

			$('#grafik1').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Grafik Pemakaian Listrik'
				},
				xAxis: {
					categories: data.blth_pemakaian
				},
				yAxis: {
					title: {
						text: 'kWh'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				series: [{
					name: 'kWh kVARH',
					data: data.kwh_kvarh
				},{
					name: 'kWh LWBP',
					data: data.kwh_lwbp
				},{
					name: 'kWh WBP',
					data: data.kwh_wbp
				},{
					name: 'kWh Total',
					data: data.kwh_total
				}]
			});

			$('#grafik2').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Grafik Tagihan Listrik'
				},
				xAxis: {
					categories: data.blth
				},
				yAxis: {
					title: {
						text: 'Rp'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				series: [{
					name: 'Rp kVARH',
					data: data.rp_kvarh
				},{
					name: 'Rp LWBP',
					data: data.rp_lwbp
				},{
					name: 'Rp WBP',
					data: data.rp_wbp
				},{
					name: 'Rp Total',
					data: data.rp_total
				}]
			});
		}
	});
});