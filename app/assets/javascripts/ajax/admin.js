$(function(){

  $(".selectArea").change(function(){
      var area_id = $(".selectArea").val();
      $.ajax({
            type: "post",
            data: "area_id="+area_id,
            url: "/json/admin/rayons/get_all",
            success: function(response){
                var data = JSON.parse(response);
                var newData = "<option value=''>-- Select Rayon --</option>";

                for(var n=0;n<data.length;n++) {
                    newData += "<option value='"+ data[n].rayon_id +"'>"+ data[n].name +"</option>"
                }

                // $('#selectRayon').show();
                $(".selectRayon").empty().append(newData);
                $('#rayongroup').show();
                $(".selectRayon").trigger("liszt:updated");
            }
       });
  });

  $(".chosen-select").chosen(); 
  $('#chosen-multiple-style').on('click', function(e){
    var target = $(e.target).find('input[type=radio]');
    var which = parseInt(target.val());
    if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
     else $('#form-field-select-4').removeClass('tag-input-style');
  });

  $('#id-input-file-1').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
});