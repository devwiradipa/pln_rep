$(function () {
	var blth_tahun_awal = $("#blth_tahun_awal").val();
	var blth_bulan_awal = $("#blth_bulan_awal").val();
	var blth_tahun_akhir = $("#blth_tahun_akhir").val();
	var blth_bulan_akhir = $("#blth_bulan_akhir").val();
	var user_id = $("#user_id").val();
	var kawasan_id = $("#kawasan_id").val();
	var rayon_id = $("#rayon_id").val();
	var area_id = $("#area_id").val();

	$.ajax({
		type: "POST",
		data: "blth_bulan_awal="+blth_bulan_awal+"&blth_tahun_awal="+blth_tahun_awal+"&blth_bulan_akhir="+blth_bulan_akhir+"&blth_tahun_akhir="+blth_tahun_akhir+"&user_id="+user_id+"&kawasan_id="+kawasan_id+"&rayon_id="+rayon_id+"&area_id="+area_id,
		url: "/json/admin/monthly_info_summaries/get_data",
		success: function(response){
			var data = response;

			$('#grafik').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Grafik Gangguan Listrik'
				},
				xAxis: {
					categories: data.summary_blth
				},
				yAxis: {
					title: {
						text: 'Kali/Jam'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				series: [{
					name: 'Durasi Gangguan',
					data: data.summary_durasi_gangguan
				},{
					name: 'Frekuensi Gangguan',
					data: data.summary_frekuensi_gangguan
				}]
			});

			$('#grafik1').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Grafik Pemakaian Listrik'
				},
				xAxis: {
					categories: data.summary_blth
				},
				yAxis: {
					title: {
						text: 'kWh'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				series: [{
					name: 'kWh kVARH',
					data: data.summary_kwh_kvarh
				},{
					name: 'kWh LWBP',
					data: data.summary_kwh_lwbp
				},{
					name: 'kWh WBP',
					data: data.summary_kwh_wbp
				},{
					name: 'kWh Total',
					data: data.summary_kwh_total
				}]
			});

			$('#grafik2').highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: 'Grafik Tagihan Listrik'
				},
				xAxis: {
					categories: data.summary_blth
				},
				yAxis: {
					title: {
						text: 'Rp'
					}
				},
				plotOptions: {
					line: {
						dataLabels: {
							enabled: true
						},
						enableMouseTracking: true
					}
				},
				series: [{
					name: 'Rp kVARH',
					data: data.summary_rp_kvarh
				},{
					name: 'Rp LWBP',
					data: data.summary_rp_lwbp
				},{
					name: 'Rp WBP',
					data: data.summary_rp_wbp
				},{
					name: 'Rp Total',
					data: data.summary_rp_total
				}]
			});
		}
	});

   $(".chosen-select").chosen(); 
        $('#chosen-multiple-style').on('click', function(e){
          var target = $(e.target).find('input[type=radio]');
          var which = parseInt(target.val());
          if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
           else $('#form-field-select-4').removeClass('tag-input-style');
        });
});