$(function(){

  $(".selectArea").change(function(){
      var area_id = $(".selectArea").val();
      $.ajax({
            type: "post",
            data: "area_id="+area_id,
            url: "/json/admin/rayons/get_all",
            success: function(response){
                var data = JSON.parse(response);
                var newData = "<option value=''>-- Select Rayon --</option>";

                for(var n=0;n<data.length;n++) {
                    newData += "<option value='"+ data[n].rayon_id +"'>"+ data[n].name +"</option>"
                }

                // $('#selectRayon').show();
                $(".selectRayon").empty().append(newData);
                $('#rayongroup').show();
                $(".selectRayon").trigger("liszt:updated");
            }
       });
  });

  $(".selectProvince").change(function(){
      var province_id = $(".selectProvince").val();
      $.ajax({
            type: "post",
            data: "province_id="+province_id,
            url: "/json/get_district",
            success: function(response){
                var data = JSON.parse(response);
                var newData = "<option value=''>-- Select District --</option>";

                for(var n=0;n<data.length;n++) {
                    newData += "<option value='"+ data[n].id +"'>"+ data[n].name +"</option>"
                }

                // $('#selectDistrict').show();
                $("#user_district_id_chosen").remove();
                $(".selectDistrict").empty().append(newData);
                $('#districtgroup').show();
                // $(".selectDistrict").chosen();
                $(".selectDistrict").trigger("liszt:updated");
            }
       });
  });

   $(".selectDistrict").change(function(){
      var district_id = $(".selectDistrict").val();
      $.ajax({
            type: "post",
            data: "district_id="+district_id,
            url: "/json/get_subdistrict",
            success: function(response){
                var data = JSON.parse(response);
                var newData = "<option value=''>-- Select Sub District --</option>";

                for(var n=0;n<data.length;n++) {
                    newData += "<option value='"+ data[n].id +"'>"+ data[n].name +"</option>"
                }

                // $('#selectSubDistrict').show();
                $("#user_subdistrict_id_chosen").remove();
                $(".selectSubDistrict").empty().append(newData);
                $('#subdistrictgroup').show();
                // $(".selectSubDistrict").chosen();
                $(".selectSubDistrict").trigger("liszt:updated");
            }
       });
  });

   $("form").bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'user[username]': {
                    validators: {
                        notEmpty: {
                            message: 'username tidak boleh kosong'
                        }
                    }
                },
                'user[email]': {
                    validators: {
                        notEmpty: {
                            message: 'email tidak boleh kosong'
                        }
                    }
                },
                'user[name]': {
                    validators: {
                        notEmpty: {
                            message: 'nama pelanggan tidak boleh kosong'
                        }
                    }
                },
                'user[idpel]': {
                    validators: {
                        notEmpty: {
                            message: 'identitas pelanggan tidak boleh kosong'
                        }
                    }
                },
                'user[password]': {
                    validators: {
                        notEmpty: {
                            message: 'password tidak boleh kosong'
                        },
                        identical: {
                            field: 'user[password_confirmation]',
                            message: 'password dan password confirmation harus sama'
                        }
                    }
                },
                'user[password_confirmation]': {
                    validators: {
                        notEmpty: {
                            message: 'password confirmation tidak boleh kosong'
                        },
                        identical: {
                            field: 'user[password]',
                            message: 'password dan password confirmation harus sama'
                        }
                    }
                }
            }
        })
        // Enable the password/confirm password validators if the password is not empty
        .on('keyup', '[name="user[password]"]', function() {
            var isEmpty = $(this).val() == '';
            $('#new_user')
                    .bootstrapValidator('enableFieldValidators', 'user[password]', !isEmpty)
                    .bootstrapValidator('enableFieldValidators', 'user[password_confirmation]', !isEmpty);

            // Revalidate the field when user start typing in the password field
            if ($(this).val().length == 1) {
                $('#new_user').bootstrapValidator('validateField', 'user[password]')
                                .bootstrapValidator('validateField', 'user[password_confirmation]');
            }
        });

   $(".chosen-select").chosen(); 
        $('#chosen-multiple-style').on('click', function(e){
          var target = $(e.target).find('input[type=radio]');
          var which = parseInt(target.val());
          if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
           else $('#form-field-select-4').removeClass('tag-input-style');
        });

  $('#id-input-file-1').ace_file_input({
          no_file:'Tidak Ada File ...',
          btn_choose:'Pilih',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });

  $('#id-input-file-2').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Pilih',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
        
        $('#id-input-file-3').ace_file_input({
          style:'well',
          btn_choose:'Drop files here or click to choose',
          btn_change:null,
          no_icon:'icon-cloud-upload',
          droppable:true,
          thumbnail:'small'//large | fit
          //,icon_remove:null//set null, to hide remove/reset button
          /**,before_change:function(files, dropped) {
            //Check an example below
            //or examples/file-upload.html
            return true;
          }*/
          /**,before_remove : function() {
            return true;
          }*/
          ,
          preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
          }
      
        }).on('change', function(){
          //console.log($(this).data('ace_input_files'));
          //console.log($(this).data('ace_input_method'));
        });

  $('.datepicker').datepicker({ autoclose:true, format: "yyyy-mm-dd" });
});