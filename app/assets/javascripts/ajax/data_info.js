$(function(){
  $('.datepicker').datepicker({ autoclose:true, format: "yyyy-mm-dd" });
  $('.timepicker').timepicker({'timeFormat': 'H:i:s', 'showDuration': true});

  $(".selectJenisInfo").change(function(){
      var jenis_info_id = $(".selectJenisInfo").val();
      $.ajax({
            type: "post",
            data: "jenis_info_id="+jenis_info_id,
            url: "/json/admin/data_info_details/get_all",
            success: function(response){
                var data = JSON.parse(response);
                var newData = "<option value=''>-- Select Detail Jenis Info --</option>";

                for(var n=0;n<data.length;n++) {
                    newData += "<option value='"+ data[n].id +"'>"+ data[n].name +"</option>"
                }

                // $('#selectDetail Jenis Info').show();
                if(data.length != 0) {
                  $(".selectDetailJenisInfo").empty().append(newData);
                  $('#detailjenisinfogroup').show();
                  $(".selectDetailJenisInfo").trigger("liszt:updated");
                }
            }
       });
  });
        
  $('#id-input-file-2').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
        
  $('.id-input-file-3').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });

  $(".chosen-select").chosen(); 
        $('#chosen-multiple-style').on('click', function(e){
          var target = $(e.target).find('input[type=checkbox]');
          var which = parseInt(target.val());
          if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
           else $('#form-field-select-4').removeClass('tag-input-style');
        });

  $('#chosen-multiple-style').on('click', function(e){
          var target = $(e.target).find('input[type=checkbox]');
          var which = parseInt(target.val());
          if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
           else $('#form-field-select-4').removeClass('tag-input-style');
        });
});
