class HistoriesController < ApplicationController
	before_filter :authenticate_user!

	def index
		if params[:blth_bulan_awal] && params[:blth_bulan_awal] != ""
			@blth_bulan_awal = params[:blth_bulan_awal].to_i
		else
			@blth_bulan_awal = Time.now.month-2
		end

		if params[:blth_bulan_akhir] && params[:blth_bulan_akhir] != ""
			@blth_bulan_akhir = params[:blth_bulan_akhir].to_i
		else
			@blth_bulan_akhir = Time.now.month
		end

		if params[:blth_tahun_awal] && params[:blth_tahun_awal] != ""
			@blth_tahun_awal = params[:blth_tahun_awal].to_i
		else
			@blth_tahun_awal = Time.now.year
		end

		if params[:blth_tahun_akhir] && params[:blth_tahun_akhir] != ""
			@blth_tahun_akhir = params[:blth_tahun_akhir].to_i
		else
			@blth_tahun_akhir = Time.now.year
		end

		if @blth_bulan_awal < 0
			@blth_bulan_awal = @blth_bulan_awal+12
			@blth_tahun_awal = @blth_tahun_awal-1
		end

		@monthly_infos = MonthlyInfo.where("user_id = ? AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
				AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
				current_user.id, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
				order("monthly_infos.tahun_blth ASC, monthly_infos.bulan_blth ASC")

  		filename = "Data Bulanan.xls" 

	    respond_to do |format|
	      format.html # index.html.erb
	      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
	      format.json { render json: @monthly_infos }
	    end
	end
end
