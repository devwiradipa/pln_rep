class TestimonialsController < ApplicationController
	layout "application_home"

	def index
		@testimonials = Testimonial.order("created_at DESC")
	end
end
