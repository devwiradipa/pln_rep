class UsersController < ApplicationController
	before_filter :authenticate_user!
	  
	def index
	   	@user = current_user
	end

  # GET /users/1/edit
  def edit
    @user = current_user
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")

    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to [:my_accounts], notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit_password
    @user = current_user
  end

  # PUT /users/1
  # PUT /users/1.json
  def update_password
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_with_password(password_params)
        format.html { redirect_to [:my_accounts], notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit_password" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:nomor_klui, :jenis_usaha, :jenis_bahan_baku, :jenis_komoditi_produk, :kapasitas_produksi_per_tahun, :jumlah_tenaga_kerja, :mesin_peralatan_utama, :mesin_peralatan_pembantu, :nama_dirut_string, :nama_wakil_dirut, :nama_contact, :jabatan_contact, :no_telepon_contact, :nomor_hp_contact, :email_contact, :nama_contact_2, :jabatan_contact_2, :no_telepon_contact_2, :nomor_hp_contact_2, :email_contact_2)
    end

    def password_params
      params.require(:user).permit(:current_password, :password, :password_confirmation)
    end
end
