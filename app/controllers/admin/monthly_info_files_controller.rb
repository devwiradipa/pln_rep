class Admin::MonthlyInfoFilesController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"

  def new
    @monthly_info_file = MonthlyInfoFile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @monthly_info_file }
    end
  end

  def create
    @monthly_info_file = MonthlyInfoFile.new(monthly_info_file_params)

    respond_to do |format|
      if @monthly_info_file.save
	    File.chmod(0777, @monthly_info_file.file.path)
	    excel_file = Roo::Excelx.new(@monthly_info_file.file.path)

        @monthly_info_file.monthly_info_processing(excel_file, @monthly_info_file.id)

        format.html { redirect_to upload_invoice_form_admin_monthly_infos_path(:monthly_info_file_id => @monthly_info_file.id), notice: 'Monthly Info File was successfully uploaded.' }
        format.json { render json: @monthly_info_file, status: :created, location: @monthly_info_file }
      else
        format.html { render action: "new" }
        format.json { render json: @monthly_info_file.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def monthly_info_file_params
    params.require(:monthly_info_file).permit(:name, :file, monthly_infos_attributes: [:bulan_blth, :tahun_blth, :durasi_gangguan, :frekuensi_gangguan, :idpel, :kvamaks, :kwh_kvarh, :kwh_lwbp, :kwh_total, :kwh_wbp, :rp_kvarh, :rp_lwbp, :rp_wbp, :rptag, :user_id, :beban_min, :beban_rata, :beban_maks, :invoice_file, :monthly_info_file_id, :_destroy])
  end
end
