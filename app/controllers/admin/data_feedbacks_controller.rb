class Admin::DataFeedbacksController < ApplicationController
	before_filter :authenticate_admin!
  before_filter :authorize_admin

  layout "admin"

	def index
    if current_admin.user_group && current_admin.user_group.super == 1
		  @data_feedbacks = DataFeedback.where("parent_id IS NULL")
    elsif current_admin.user_group && current_admin.kawasan
      if Kawasan.where("id = ?", current_admin.kawasan_id).first
        kawasan = Kawasan.where("id = ?", current_admin.kawasan_id).first
        @users = User.where("kawasan_id = ?", kawasan.id)
        @data_feedbacks = DataFeedback.where("parent_id IS NULL AND user_id IN (?)", @users.map(&:id))
      else
        @data_feedbacks = DataFeedback.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.rayon
      if Rayon.where("rayon_id = ?", current_admin.rayon_new_id).first
        rayon = Rayon.where("rayon_id = ?", current_admin.rayon_new_id).first
        @users = User.where("rayon_new_id = ?", rayon.rayon_id)
        @data_feedbacks = DataFeedback.where("parent_id IS NULL AND user_id IN (?)", @users.map(&:id))
      else
        @data_feedbacks = DataFeedback.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      if Area.where("area_id = ?", current_admin.area_new_id).first
        area = Area.where("area_id = ?", current_admin.area_new_id).first
        @users = User.where("area_new_id = ?", area.area_id)
        @data_feedbacks = DataFeedback.where("parent_id IS NULL AND user_id IN (?)", @users.map(&:id))
      else
        @data_feedbacks = DataFeedback.where("id IS NULL")
      end
    else
      @data_feedbacks = DataFeedback.where("id IS NULL")
    end

    if params[:blth_bulan_awal] && params[:blth_bulan_awal] != ""
      @blth_bulan_awal = params[:blth_bulan_awal].to_i
    else
      @blth_bulan_awal = 1
    end

    if params[:blth_bulan_akhir] && params[:blth_bulan_akhir] != ""
      @blth_bulan_akhir = params[:blth_bulan_akhir].to_i
    else
      @blth_bulan_akhir = 12
    end

    if params[:blth_tahun_awal] && params[:blth_tahun_awal] != ""
      @blth_tahun_awal = params[:blth_tahun_awal].to_i
    else
      @blth_tahun_awal = 1970
    end

    if params[:blth_tahun_akhir] && params[:blth_tahun_akhir] != ""
      @blth_tahun_akhir = params[:blth_tahun_akhir].to_i
    else
      @blth_tahun_akhir = 3000
    end

    if params[:feedback_type_id] && params[:feedback_type_id] != ""
      @data_feedbacks = DataFeedback.where("id IN (?) AND feedback_type_id = ? AND ((MONTH(data_feedbacks.created_at) >= ? AND YEAR(data_feedbacks.created_at) >= ?) OR (MONTH(data_feedbacks.created_at) < ? AND YEAR(data_feedbacks.created_at) > ?)) 
        AND ((MONTH(data_feedbacks.created_at) <= ? AND YEAR(data_feedbacks.created_at) <= ?) OR (MONTH(data_feedbacks.created_at) > ? AND YEAR(data_feedbacks.created_at < ?)))", 
        @data_feedbacks.map(&:id), params[:feedback_type_id].to_i, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
        order("data_feedbacks.created_at DESC" )
    else
      @data_feedbacks = DataFeedback.where("id IN (?) AND ((MONTH(data_feedbacks.created_at) >= ? AND YEAR(data_feedbacks.created_at) >= ?) OR (MONTH(data_feedbacks.created_at) < ? AND YEAR(data_feedbacks.created_at) > ?)) 
        AND ((MONTH(data_feedbacks.created_at) <= ? AND YEAR(data_feedbacks.created_at) <= ?) OR (MONTH(data_feedbacks.created_at) > ? AND YEAR(data_feedbacks.created_at < ?)))", 
        @data_feedbacks.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
        order("data_feedbacks.created_at DESC" )
    end

    @feedback_types = FeedbackType.all
	end
  
  def new
    @data_feedback = DataFeedback.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_feedback }
    end
  end
  
  def show
    @data_feedback = DataFeedback.find(params[:id])
    @data_feedback.update_attributes(read: 1, read_by: current_admin.id, read_at: Time.now)
    @data_feedback.replies.each do |reply|
      reply.update_attributes(read: 1, read_by: current_admin.id, read_at: Time.now)
    end

    @new_data_feedback = DataFeedback.new

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_feedback }
    end
  end
  
  def create
    @data_feedback = DataFeedback.new(data_feedback_params)
    @data_feedback.read = 1
    @data_feedback.read_by = current_admin.id
    @data_feedback.read_at = Time.now

    respond_to do |format|
      if @data_feedback.save
        @data_feedback.parent.update_attributes(:status_feedback => "sudah dibalas")

        # UserMailer.reply_notification(@data_feedback)
        # sidekiq can only use non object parameter
        receiver_name = @data_feedback.parent.user.name
        feedback_plg = @data_feedback.feedback_plg
        created_by = @data_feedback.admin.name
        id = @data_feedback.parent_id.to_s
        email = @data_feedback.parent.user.email
        UserMailer.reply_notification(created_by, feedback_plg, receiver_name, id, email, @hostname).deliver

        if @data_feedback.parent.user.kawasan
          @data_feedback.parent.user.kawasan.admins.each do |admin|
            if admin.email
              receiver_name = admin.name
              feedback_plg = @data_feedback.feedback_plg
              id = @data_feedback.parent_id.to_s
              email = admin.email
              user_name = @data_feedback.parent.user.name

              if @data_feedback.admin
                if @data_feedback.admin.kawasan
                  created_by = @data_feedback.admin.name+" (Pengelola Kawasan "+ @data_feedback.admin.kawasan.nama_kawasan+")"
                elsif @data_feedback.admin.rayon
                  created_by = @data_feedback.admin.name+" (PLN Rayon "+ @data_feedback.admin.rayon.name+")"
                elsif @data_feedback.admin.area
                  created_by = @data_feedback.admin.name+" (PLN Area "+ @data_feedback.admin.area.name+")"
                else
                  created_by = @data_feedback.admin.name+" (PLN Kantor Distribusi)"
                end
              end

              UserMailer.admin_reply_notification(user_name, created_by, feedback_plg, receiver_name, id, admin.email, @hostname).deliver
            end
          end
        end

        if @data_feedback.parent.user.rayon
          @data_feedback.parent.user.rayon.admins.each do |admin|
            if admin.email
              receiver_name = admin.name
              feedback_plg = @data_feedback.feedback_plg
              user_name = @data_feedback.parent.user.name
              
              if @data_feedback.admin
                if @data_feedback.admin.kawasan
                  created_by = @data_feedback.admin.name+" (Pengelola Kawasan "+ @data_feedback.admin.kawasan.nama_kawasan+")"
                elsif @data_feedback.admin.rayon
                  created_by = @data_feedback.admin.name+" (PLN Rayon "+ @data_feedback.admin.rayon.name+")"
                elsif @data_feedback.admin.area
                  created_by = @data_feedback.admin.name+" (PLN Area "+ @data_feedback.admin.area.name+")"
                else
                  created_by = @data_feedback.admin.name+" (PLN Kantor Distribusi)"
                end
              end

              id = @data_feedback.parent_id.to_s
              email = admin.email
              UserMailer.admin_reply_notification(user_name, created_by, feedback_plg, receiver_name, id, admin.email, @hostname).deliver
            end
          end
        end

        if @data_feedback.parent.user.area
          @data_feedback.parent.user.area.admins.each do |admin|
            if admin.email
              receiver_name = admin.name
              feedback_plg = @data_feedback.feedback_plg
              user_name = @data_feedback.parent.user.name
              
              if @data_feedback.admin
                if @data_feedback.admin.kawasan
                  created_by = @data_feedback.admin.name+" (Pengelola Kawasan "+ @data_feedback.admin.kawasan.nama_kawasan+")"
                elsif @data_feedback.admin.rayon
                  created_by = @data_feedback.admin.name+" (PLN Rayon "+ @data_feedback.admin.rayon.name+")"
                elsif @data_feedback.admin.area
                  created_by = @data_feedback.admin.name+" (PLN Area "+ @data_feedback.admin.area.name+")"
                else
                  created_by = @data_feedback.admin.name+" (PLN Kantor Distribusi)"
                end
              end

              id = @data_feedback.parent_id.to_s
              email = admin.email
              UserMailer.admin_reply_notification(user_name, created_by, feedback_plg, receiver_name, id, admin.email, @hostname).deliver
            end
          end
        end

        @super = UserGroup.where("super = 1")
        @admin_kds = Admin.where("user_group_id IN (?)", @super.map(&:id))

        @admin_kds.each do |admin|
          if admin.email
            receiver_name = admin.name
            feedback_plg = @data_feedback.feedback_plg
            user_name = @data_feedback.parent.user.name
            
            if @data_feedback.admin
              if @data_feedback.admin.kawasan
                created_by = @data_feedback.admin.name+" (Pengelola Kawasan "+ @data_feedback.admin.kawasan.nama_kawasan+")"
              elsif @data_feedback.admin.rayon
                created_by = @data_feedback.admin.name+" (PLN Rayon "+ @data_feedback.admin.rayon.name+")"
              elsif @data_feedback.admin.area
                created_by = @data_feedback.admin.name+" (PLN Area "+ @data_feedback.admin.area.name+")"
              else
                created_by = @data_feedback.admin.name+" (PLN Kantor Distribusi)"
              end
            end

            id = @data_feedback.parent_id.to_s
            email = admin.email
            UserMailer.admin_reply_notification(user_name, created_by, feedback_plg, receiver_name, id, admin.email, @hostname).deliver
          end
        end

      	format.html { redirect_to(admin_data_feedback_path(@data_feedback.parent), :notice => 'data_feedback was successfully created.') }
        format.xml  { render :xml => @data_feedback, :status => :created, :data_feedback => @data_feedback }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_feedback.errors, :status => :unprocessable_entity }
      end
    end
  end

  def close
    @data_feedback = DataFeedback.find(params[:id])
    @data_feedback.update_attributes(:status_feedback => "closed", :closed_time => Time.now)

    redirect_to :back
  end

  private

  def data_feedback_params
    params.require(:data_feedback).permit(:feedback_plg, :idpel, :parent_id, :tgl_feedback, :user_id, :status_feedback, :admin_id, :read, :user_read, :closed_time, :feedback_type_id)
  end
end
