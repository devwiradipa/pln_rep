class Admin::MyAccountsController < ApplicationController
	before_filter :authenticate_admin!
    
	layout "admin"
	  
	def index
	   	@admin = current_admin
	end
  
  	def edit
	    @my_account = current_admin
	    @user_groups = UserGroup.order("name ASC")
	    @rayons = Rayon.order("name ASC")
	    @areas = Area.order("name ASC")
	    @kawasans = Kawasan.order("nama_kawasan ASC")
  	end
	  
  	def update
	    @my_account = current_admin
	    @user_groups = UserGroup.order("name ASC")
	    @rayons = Rayon.order("name ASC")
	    @areas = Area.order("name ASC")
	    @kawasans = Kawasan.order("nama_kawasan ASC")

	    respond_to do |format|
	      if @my_account.update_attributes(my_account_params)
	        
	        format.html { redirect_to(admin_my_accounts_path, :notice => 'admin was successfully updated.') }
	        format.xml  { head :ok }
	      else
	        format.html { render :action => "edit" }
	        format.xml  { render :xml => @my_account.errors, :status => :unprocessable_entity }
	      end
	    end
	end

  	private

  	def my_account_params
	    params.require(:admin).permit(:email, :username, :name, :user_group_id, :rayon_id, :area_id, :rayon_new_id, :area_new_id, :kawasan_id)
  	end
end
