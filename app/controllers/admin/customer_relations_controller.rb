class Admin::CustomerRelationsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @customer_relations = CustomerRelation.order("relation_date DESC")
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")

    @customer_relation_types = CustomerRelationType.all

    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
  end
  
  def new
    @customer_relation = CustomerRelation.new
    @customer_relation_types = CustomerRelationType.all
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @customer_relation }
    end
  end
  
  def show
    @customer_relation = CustomerRelation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @customer_relation }
    end
  end
  
  def create
    @customer_relation = CustomerRelation.new(customer_relation_params)
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    @customer_relation_types = CustomerRelationType.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      if @customer_relation.save
        
        format.html { redirect_to(admin_customer_relations_path, :notice => 'customer_relation was successfully created.') }
        format.xml  { render :xml => @customer_relation, :status => :created, :customer_relation => @customer_relation }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @customer_relation.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @customer_relation = CustomerRelation.find(params[:id])
    @customer_relation_types = CustomerRelationType.all
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
  end
  
  def update
    @customer_relation = CustomerRelation.find(params[:id])
    @customer_relation_types = CustomerRelationType.all
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      if @customer_relation.update_attributes(customer_relation_params)
        
        format.html { redirect_to(admin_customer_relations_path, :notice => 'customer_relation was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @customer_relation.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @customer_relation = CustomerRelation.find(params[:id])
    @customer_relation.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/customer_relations') }
      format.xml  { head :ok }
    end
  end

  private

  def customer_relation_params
    params.require(:customer_relation).permit(:customer_relation_type_id, :description, :relation_date, :relation_time, :user_id, :created_by, :updated_by)
  end
end
