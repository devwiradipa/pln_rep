class Admin::MonthlyInfosController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
  
  

  layout 'admin'
  # GET /monthly_infos
  # GET /monthly_infos.json
  def index
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
      @areas = Area.order("name ASC")
      @kawasans = Kawasan.order("nama_kawasan ASC")
      @rayons = Rayon.order("name ASC")
      @monthly_infos = MonthlyInfo.order("tahun_blth DESC, bulan_blth DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @areas = Area.where("id IS NULL")
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
        @rayons = Rayon.where("id IS NULL")
        @monthly_infos = MonthlyInfo.where("user_id IN (?)", @users.map(&:id)).order("tahun_blth DESC, bulan_blth DESC")
      else
        @users = User.where("id IS NULL") 
        @areas = Area.where("id IS NULL")
        @rayons = Rayon.where("id IS NULL")
        @kawasans = Kawasan.where("id IS NULL")
        @monthly_infos = MonthlyInfo.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
        @monthly_infos = MonthlyInfo.where("user_id IN (?)", @users.map(&:id)).order("tahun_blth DESC, bulan_blth DESC")
      else
        @users = User.where("id IS NULL") 
        @areas = Area.where("id IS NULL")
        @rayons = Rayon.where("id IS NULL")
        @kawasans = Kawasan.where("id IS NULL")
        @monthly_infos = MonthlyInfo.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
        @monthly_infos = MonthlyInfo.where("user_id IN (?)", @users.map(&:id)).order("tahun_blth DESC, bulan_blth DESC")
      else
        @users = User.where("id IS NULL") 
        @areas = Area.where("id IS NULL")
        @rayons = Rayon.where("id IS NULL")
        @kawasans = Kawasan.where("id IS NULL")
        @monthly_infos = MonthlyInfo.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL") 
      @areas = Area.where("id IS NULL")
      @rayons = Rayon.where("id IS NULL")
      @kawasans = Kawasan.where("id IS NULL")
      @monthly_infos = MonthlyInfo.where("id IS NULL")
    end

    if params[:user_id] && params[:user_id] != ""
      @user = User.find(params[:user_id].to_i)
      @filtered_users = User.where(:id => @user)
    elsif params[:kawasan_id] && params[:kawasan_id] != ""
      @kawasan = Kawasan.find(params[:kawasan_id])
      @filtered_users = User.where(:id => @kawasan.users)
    elsif params[:rayon_id] && params[:rayon_id] != ""
      @rayon = Rayon.find(params[:rayon_id])
      @filtered_users = User.where(:id => @rayon.users)
    elsif params[:area_id] && params[:area_id] != ""
      @area = Area.find(params[:area_id])
      @filtered_users = User.where(:id => @area.users)
    else
      @filtered_users = User.all
    end

    if params[:blth_bulan_awal] && params[:blth_bulan_awal] != ""
      @blth_bulan_awal = params[:blth_bulan_awal].to_i
    else
      @blth_bulan_awal = 1
    end

    if params[:blth_bulan_akhir] && params[:blth_bulan_akhir] != ""
      @blth_bulan_akhir = params[:blth_bulan_akhir].to_i
    else
      @blth_bulan_akhir = 12
    end

    if params[:blth_tahun_awal] && params[:blth_tahun_awal] != ""
      @blth_tahun_awal = params[:blth_tahun_awal].to_i
    else
      @blth_tahun_awal = 1970
    end

    if params[:blth_tahun_akhir] && params[:blth_tahun_akhir] != ""
      @blth_tahun_akhir = params[:blth_tahun_akhir].to_i
    else
      @blth_tahun_akhir = 3000
    end

    # if @blth_bulan_awal < 0
    #   @blth_bulan_awal = @blth_bulan_awal+12
    #   @blth_tahun_awal = @blth_tahun_awal-1
    # end

    @monthly_infos = MonthlyInfo.where(
      "id IN (?) AND user_id IN (?) AND ((bulan_blth >= ? AND tahun_blth >= ?) OR (bulan_blth < ? AND tahun_blth > ?)) 
        AND ((bulan_blth <= ? AND tahun_blth <= ?) OR (bulan_blth > ? AND tahun_blth < ?))", 
        @monthly_infos.map(&:id), @filtered_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).order(
        "tahun_blth DESC, bulan_blth DESC")

  filename = "Data Bulanan.xls" 
    respond_to do |format|
      format.html # index.html.erb
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
      format.json { render json: @monthly_infos }
    end
  end

  # GET /monthly_infos/1
  # GET /monthly_infos/1.json
  def show
    @monthly_info = MonthlyInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @monthly_info }
    end
  end

  # GET /monthly_infos/new
  # GET /monthly_infos/new.json
  def new
    @monthly_info = MonthlyInfo.new
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @monthly_info }
    end
  end

  # GET /monthly_infos/1/edit
  def edit
    @monthly_info = MonthlyInfo.find(params[:id])
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
  end

  # POST /monthly_infos
  # POST /monthly_infos.json
  def create
    @monthly_info = MonthlyInfo.new(monthly_info_params)
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    @monthly_info.rptag = params[:monthly_info][:rp_lwbp].to_i + params[:monthly_info][:rp_wbp].to_i + params[:monthly_info][:rp_kvarh].to_i
    @monthly_info.kwh_total = params[:monthly_info][:kwh_lwbp].to_i + params[:monthly_info][:kwh_wbp].to_i

    user = User.find(params[:monthly_info][:user_id].to_i)
    
    if @monthly_info.kwh_total && params[:monthly_info][:kwh_kvarh] && @monthly_info.kwh_total == 0 && params[:monthly_info][:kwh_kvarh] == 0
      @monthly_info.beban_min = @monthly_info.kwh_total / Math.sqrt(@monthly_info.kwh_total*@monthly_info.kwh_total+params[:monthly_info][:kwh_kvarh].to_i*params[:monthly_info][:kwh_kvarh].to_i)
    else
      @monthly_info.beban_min = 0
    end

    if @monthly_info.kwh_total
      @monthly_info.beban_rata = @monthly_info.kwh_total / 720.to_f
    end

    if @monthly_info.beban_maks
      @monthly_info.beban_maks = params[:monthly_info][:kvamaks].to_i * user.faktor_kali_meter.to_i
    end
    
    respond_to do |format|
      monthly_info = MonthlyInfo.where("user_id = ? AND bulan_blth = ? AND tahun_blth = ?", user.id, params[:monthly_info][:bulan_blth].to_i, params[:monthly_info][:tahun_blth].to_i).first
      if monthly_info
        if params[:monthly_info][:rp_lwbp] && params[:monthly_info][:rp_wbp] && params[:monthly_info][:rp_kvarh]
          params[:monthly_info][:rptag] = params[:monthly_info][:rp_lwbp].to_i + params[:monthly_info][:rp_wbp].to_i + params[:monthly_info][:rp_kvarh].to_i
        else
          params[:monthly_info][:rptag] = 0
        end
        if params[:monthly_info][:kwh_lwbp] && params[:monthly_info][:kwh_wbp]
          params[:monthly_info][:kwh_total] = params[:monthly_info][:kwh_lwbp].to_i + params[:monthly_info][:kwh_wbp].to_i
        else
          params[:monthly_info][:kwh_total] = 0
        end
        if params[:monthly_info][:kwh_total] && params[:monthly_info][:kwh_kvarh] && params[:monthly_info][:kwh_kvarh] != 0 && params[:monthly_info][:kwh_total] != 0
          params[:monthly_info][:beban_min] = params[:monthly_info][:kwh_total].to_i / Math.sqrt(params[:monthly_info][:kwh_total].to_i*params[:monthly_info][:kwh_total].to_i+params[:monthly_info][:kwh_kvarh].to_i*params[:monthly_info][:kwh_kvarh].to_i)
        else
          params[:monthly_info][:beban_min] = 0
        end
        if params[:monthly_info][:kwh_total]
    		  params[:monthly_info][:beban_rata] = params[:monthly_info][:kwh_total].to_i / 720.to_f
        else
          params[:monthly_info][:beban_rata] = 0
        end
        if params[:monthly_info][:kvamaks]
      		params[:monthly_info][:beban_maks] = params[:monthly_info][:kvamaks].to_i * user.faktor_kali_meter.to_i
        else
          params[:monthly_info][:beban_maks] = 0
        end

        monthly_info.update_attributes(params[:monthly_info])

        if monthly_info.user
          if monthly_info.user.area
            @areas_data_infos = AreasDataInfo.where(:area_id => monthly_info.user.area.id)
          else
            @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")
          end

          @kawasans_data_infos = KawasansDataInfo.where(:kawasan_id => monthly_info.user.kawasan_id)

          @users_data_infos = UsersDataInfo.where(:user_id => monthly_info.user_id)
        else
          @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")

          @kawasans_data_infos = KawasansDataInfo.where("data_info_id IS NULL")

          @users_data_infos = UsersDataInfo.where("data_info_id IS NULL")
        end

        @data_infos = DataInfo.where("jenis_info_id = 1 AND MONTH(tgl_info_gangguan) = ? AND YEAR(tgl_info_gangguan) = ? AND (id IN (?) OR id IN (?) OR id IN (?))", monthly_info.bulan_blth, monthly_info.tahun_blth, @areas_data_infos.map(&:data_info_id), @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id))

        monthly_info.update_attributes(:durasi_gangguan => @data_infos.sum(&:duration).to_f, :frekuensi_gangguan => @data_infos.count )

        format.html { redirect_to [:admin,monthly_info], notice: 'MonthlyInfo was successfully created.' }
        format.json { render json: monthly_info, status: :created, location: monthly_info }

      elsif @monthly_info.save

        if params[:monthly_info][:rp_lwbp] && params[:monthly_info][:rp_wbp] && params[:monthly_info][:rp_kvarh]
          params[:monthly_info][:rptag] = params[:monthly_info][:rp_lwbp].to_i + params[:monthly_info][:rp_wbp].to_i + params[:monthly_info][:rp_kvarh].to_i
        else
          params[:monthly_info][:rptag] = 0
        end
        if params[:monthly_info][:kwh_lwbp] && params[:monthly_info][:kwh_wbp]
          params[:monthly_info][:kwh_total] = params[:monthly_info][:kwh_lwbp].to_i + params[:monthly_info][:kwh_wbp].to_i
        else
          params[:monthly_info][:kwh_total] = 0
        end
        if params[:monthly_info][:kwh_total] && params[:monthly_info][:kwh_kvarh] && params[:monthly_info][:kwh_kvarh] != 0 && params[:monthly_info][:kwh_total] != 0
          params[:monthly_info][:beban_min] = params[:monthly_info][:kwh_total].to_i / Math.sqrt(params[:monthly_info][:kwh_total].to_i*params[:monthly_info][:kwh_total].to_i+params[:monthly_info][:kwh_kvarh].to_i*params[:monthly_info][:kwh_kvarh].to_i)
        else
          params[:monthly_info][:beban_min] = 0
        end
        if params[:monthly_info][:kwh_total]
          params[:monthly_info][:beban_rata] = params[:monthly_info][:kwh_total].to_i / 720.to_f
        else
          params[:monthly_info][:beban_rata] = 0
        end
        if params[:monthly_info][:kvamaks]
          params[:monthly_info][:beban_maks] = params[:monthly_info][:kvamaks].to_i * user.faktor_kali_meter.to_i
        else
          params[:monthly_info][:beban_maks] = 0
        end

        monthly_info.update_attributes(params[:monthly_info])

        if @monthly_info.user
          if @monthly_info.user.area
            @areas_data_infos = AreasDataInfo.where(:area_id => @monthly_info.user.area.id)
          else
            @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")
          end

          @kawasans_data_infos = KawasansDataInfo.where(:kawasan_id => @monthly_info.user.kawasan_id)

          @users_data_infos = UsersDataInfo.where(:user_id => @monthly_info.user_id)
        else
          @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")

          @kawasans_data_infos = KawasansDataInfo.where("data_info_id IS NULL")

          @users_data_infos = UsersDataInfo.where("data_info_id IS NULL")
        end

        @data_infos = DataInfo.where("jenis_info_id = 1 AND MONTH(tgl_info_gangguan) = ? AND YEAR(tgl_info_gangguan) = ? AND (id IN (?) OR id IN (?) OR id IN (?))", @monthly_info.bulan_blth, @monthly_info.tahun_blth, @areas_data_infos.map(&:data_info_id), @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id))

        @monthly_info.update_attributes(:durasi_gangguan => @data_infos.sum(&:duration).to_f, :frekuensi_gangguan => @data_infos.count )

        format.html { redirect_to [:admin,@monthly_info], notice: 'MonthlyInfo was successfully created.' }
        format.json { render json: @monthly_info, status: :created, location: @monthly_info }
      else
        format.html { render action: "new" }
        format.json { render json: @monthly_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /monthly_infos/1
  # PUT /monthly_infos/1.json
  def update
    @monthly_info = MonthlyInfo.find(params[:id])
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
    
    user = User.find(params[:monthly_info][:user_id].to_i)

    if params[:monthly_info][:rp_lwbp] && params[:monthly_info][:rp_wbp] && params[:monthly_info][:rp_kvarh]
      params[:monthly_info][:rptag] = params[:monthly_info][:rp_lwbp].to_i + params[:monthly_info][:rp_wbp].to_i + params[:monthly_info][:rp_kvarh].to_i
    else
      params[:monthly_info][:rptag] = 0
    end
    if params[:monthly_info][:kwh_lwbp] && params[:monthly_info][:kwh_wbp]
      params[:monthly_info][:kwh_total] = params[:monthly_info][:kwh_lwbp].to_i + params[:monthly_info][:kwh_wbp].to_i
    else
      params[:monthly_info][:kwh_total] = 0
    end
    if params[:monthly_info][:kwh_total] && params[:monthly_info][:kwh_kvarh] && params[:monthly_info][:kwh_kvarh] != 0 && params[:monthly_info][:kwh_total] != 0
      params[:monthly_info][:beban_min] = params[:monthly_info][:kwh_total].to_i / Math.sqrt(params[:monthly_info][:kwh_total].to_i*params[:monthly_info][:kwh_total].to_i+params[:monthly_info][:kwh_kvarh].to_i*params[:monthly_info][:kwh_kvarh].to_i)
    else
      params[:monthly_info][:beban_min] = 0
    end
    if params[:monthly_info][:kwh_total]
      params[:monthly_info][:beban_rata] = params[:monthly_info][:kwh_total].to_i / 720.to_f
    else
      params[:monthly_info][:beban_rata] = 0
    end
    if params[:monthly_info][:kvamaks]
      params[:monthly_info][:beban_maks] = params[:monthly_info][:kvamaks].to_i * user.faktor_kali_meter.to_i
    else
      params[:monthly_info][:beban_maks] = 0
    end
    

    respond_to do |format|
      if @monthly_info.update_attributes(monthly_info_params)

        if @monthly_info.user
          if @monthly_info.user.area
            @areas_data_infos = AreasDataInfo.where(:area_id => @monthly_info.user.area.id)
          else
            @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")
          end

          @kawasans_data_infos = KawasansDataInfo.where(:kawasan_id => @monthly_info.user.kawasan_id)

          @users_data_infos = UsersDataInfo.where(:user_id => @monthly_info.user_id)
        else
          @areas_data_infos = AreasDataInfo.where("data_info_id IS NULL")

          @kawasans_data_infos = KawasansDataInfo.where("data_info_id IS NULL")

          @users_data_infos = UsersDataInfo.where("data_info_id IS NULL")
        end

        @data_infos = DataInfo.where("jenis_info_id = 1 AND MONTH(tgl_info_gangguan) = ? AND YEAR(tgl_info_gangguan) = ? AND (id IN (?) OR id IN (?) OR id IN (?))", @monthly_info.bulan_blth, @monthly_info.tahun_blth, @areas_data_infos.map(&:data_info_id), @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id))

        @monthly_info.update_attributes(:durasi_gangguan => @data_infos.sum(&:duration).to_f, :frekuensi_gangguan => @data_infos.count)

        format.html { redirect_to [:admin,@monthly_info], notice: 'MonthlyInfo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @monthly_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /monthly_infos/1
  # DELETE /monthly_infos/1.json
  def destroy
    @monthly_info = MonthlyInfo.find(params[:id])
    @monthly_info.destroy

    respond_to do |format|
      format.html { redirect_to admin_monthly_infos_url }
      format.json { head :no_content }
    end
  end
  
  def upload_invoice_form
    @monthly_info_file = MonthlyInfoFile.find(params[:monthly_info_file_id])
    
  end
  
  def upload_invoice
    @monthly_info_file = MonthlyInfoFile.find(params[:monthly_info_file_id])
    
    respond_to do |format|
      if @monthly_info_file.update_attributes(monthly_info_file_params)
        format.html { redirect_to admin_monthly_infos_path, notice: 'Upload Invoices was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "upload_invoice_form" }
        format.json { render json: @monthly_info_file.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def monthly_info_params
    params.require(:monthly_info).permit(:bulan_blth, :tahun_blth, :durasi_gangguan, :frekuensi_gangguan, :idpel, :kvamaks, :kwh_kvarh, :kwh_lwbp, :kwh_total, :kwh_wbp, :rp_kvarh, :rp_lwbp, :rp_wbp, :rptag, :user_id, :beban_min, :beban_rata, :beban_maks, :invoice_file, :monthly_info_file_id)
  end

  def monthly_info_file_params
    params.require(:monthly_info_file).permit(:name, :file, monthly_infos_attributes: [:bulan_blth, :tahun_blth, :durasi_gangguan, :frekuensi_gangguan, :idpel, :kvamaks, :kwh_kvarh, :kwh_lwbp, :kwh_total, :kwh_wbp, :rp_kvarh, :rp_lwbp, :rp_wbp, :rptag, :user_id, :beban_min, :beban_rata, :beban_maks, :invoice_file, :monthly_info_file_id, :_destroy])
  end
end
