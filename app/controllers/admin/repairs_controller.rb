class Admin::RepairsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @repairs = Repair.order("created_at DESC")
  end
  
  def new
      @repair = Repair.new
	  @officers = Officer.order("name ASC")
	  @users = User.order("name ASC")
		
	  respond_to do |format|
		format.html # new.html.erb
		format.xml  { render :xml => @repair }
	  end
  end
  
  def show
    @repair = Repair.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @repair }
    end
  end
  
  def create
    @repair = Repair.new(repair_params)
    @users = User.order("name ASC")
    @officers = Officer.order("name ASC")

    respond_to do |format|
      if @repair.save
        
        format.html { redirect_to(admin_repair_path(@repair), :notice => 'repair was successfully created.') }
        format.xml  { render :xml => @repair, :status => :created, :repair => @repair }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @repair.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @repair = Repair.find(params[:id])
    @officers = Officer.order("name ASC")
  end
  
  def update
    @repair = Repair.find(params[:id])
    @officers = Officer.order("name ASC")

    respond_to do |format|
      if @repair.update_attributes(repair_params)
        format.html { redirect_to(admin_repair_path(@repair), :notice => 'repair was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @repair.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @repair = Repair.find(params[:id])
    @repair.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/repairs') }
      format.xml  { head :ok }
    end
  end

  private

  def repair_params
    params.require(:repair).permit(:examination_id, :user_id, :created_by, :updated_by, repairs_examination_items_attributes: [:examination_item_id, :note, :officer_id, :repair_id, :repair_time, :_destroy])
  end
end
