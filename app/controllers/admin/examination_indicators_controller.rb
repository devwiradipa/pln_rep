class Admin::ExaminationIndicatorsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @examination_indicators = ExaminationIndicator.order("name DESC")
  end
  
  def new
    @examination_indicator = ExaminationIndicator.new
    @examination_items = ExaminationItem.all
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @examination_indicator }
    end
  end
  
  def show
    @examination_indicator = ExaminationIndicator.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @examination_indicator }
    end
  end
  
  def create
    @examination_indicator = ExaminationIndicator.new(examination_indicator_params)
    @examination_items = ExaminationItem.all

    respond_to do |format|
      if @examination_indicator.save
        
        format.html { redirect_to(admin_examination_indicators_path, :notice => 'examination_indicator was successfully created.') }
        format.xml  { render :xml => @examination_indicator, :status => :created, :examination_indicator => @examination_indicator }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @examination_indicator.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @examination_indicator = ExaminationIndicator.find(params[:id])
    @examination_items = ExaminationItem.all
  end
  
  def update
    @examination_indicator = ExaminationIndicator.find(params[:id])
    @examination_items = ExaminationItem.all

    respond_to do |format|
      if @examination_indicator.update_attributes(examination_indicator_params)
        
        format.html { redirect_to(admin_examination_indicators_path, :notice => 'examination_indicator was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @examination_indicator.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @examination_indicator = ExaminationIndicator.find(params[:id])
    @examination_indicator.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/examination_indicators') }
      format.xml  { head :ok }
    end
  end

  def active
    @examination_indicator = ExaminationIndicator.find(params[:id])
    @examination_indicators = ExaminationIndicator.all
    @examination_indicators.each do |examination_indicator|
      examination_indicator.update_attributes(:status => 0)
    end

    @examination_indicator.update_attributes(:status => 1)

    redirect_to :back
  end

  private

  def examination_indicator_params
    params.require(:examination_indicator).permit(:created_by, :name, :status, :updated_by, :examination_item_ids)
  end
end
