class Admin::ExaminationItemsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @examination_items = ExaminationItem.order("name DESC")
  end
  
  def new
    @examination_item = ExaminationItem.new
    @examination_items = ExaminationItem.order("name DESC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @examination_item }
    end
  end
  
  def show
    @examination_item = ExaminationItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @examination_item }
    end
  end
  
  def create
    @examination_item = ExaminationItem.new(examination_item_params)
    @examination_items = ExaminationItem.order("name DESC")

    respond_to do |format|
      if @examination_item.save
        
        format.html { redirect_to(admin_examination_items_path, :notice => 'examination_item was successfully created.') }
        format.xml  { render :xml => @examination_item, :status => :created, :examination_item => @examination_item }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @examination_item.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @examination_item = ExaminationItem.find(params[:id])
    @examination_items = ExaminationItem.order("name DESC")
  end
  
  def update
    @examination_item = ExaminationItem.find(params[:id])
    @examination_items = ExaminationItem.order("name DESC")

    respond_to do |format|
      if @examination_item.update_attributes(examination_item_params)
        
        format.html { redirect_to(admin_examination_items_path, :notice => 'examination_item was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @examination_item.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @examination_item = ExaminationItem.find(params[:id])
    @examination_item.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/examination_items') }
      format.xml  { head :ok }
    end
  end

  private

  def examination_item_params
    params.require(:examination_item).permit(:created_by, :name, :updated_by, :parent_id)
  end
end
