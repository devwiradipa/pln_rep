class Admin::FeedbackTypesController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @feedback_types = FeedbackType.order("name DESC")
  end
  
  def new
    @feedback_type = FeedbackType.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @feedback_type }
    end
  end
  
  def show
    @feedback_type = FeedbackType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @feedback_type }
    end
  end
  
  def create
    @feedback_type = FeedbackType.new(feedback_type_params)

    respond_to do |format|
      if @feedback_type.save
        
        format.html { redirect_to(admin_feedback_types_path, :notice => 'feedback_type was successfully created.') }
        format.xml  { render :xml => @feedback_type, :status => :created, :feedback_type => @feedback_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @feedback_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @feedback_type = FeedbackType.find(params[:id])
  end
  
  def update
    @feedback_type = FeedbackType.find(params[:id])

    respond_to do |format|
      if @feedback_type.update_attributes(feedback_type_params)
        
        format.html { redirect_to(admin_feedback_types_path, :notice => 'feedback_type was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @feedback_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @feedback_type = FeedbackType.find(params[:id])
    @feedback_type.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/feedback_types') }
      format.xml  { head :ok }
    end
  end

  private

  def feedback_type_params
    params.require(:feedback_type).permit(:created_by, :name, :type_status, :updated_by)
  end
end
