class Admin::DataInfoDetailsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def new
    @data_info_detail = DataInfoDetail.new
    if params[:jenis_info_id]
      @jenis_info = JenisInfo.find(params[:jenis_info_id].to_i)
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_info_detail }
    end
  end
  
  def create
    @data_info_detail = DataInfoDetail.new(data_info_detail_params)
    if params[:data_info_detail][:jenis_info_id]
      @jenis_info = JenisInfo.find(params[:data_info_detail][:jenis_info_id].to_i)
    end

    respond_to do |format|
      if @data_info_detail.save
        
        format.html { redirect_to(admin_jenis_info_path(@data_info_detail.jenis_info_id), :notice => 'DataInfoDetail was successfully created.') }
        format.xml  { render :xml => @data_info_detail, :status => :created, :data_info_detail => @data_info_detail }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_info_detail.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @data_info_detail = DataInfoDetail.find(params[:id])
    @jenis_info = @data_info_detail.jenis_info
  end
  
  def update
    @data_info_detail = DataInfoDetail.find(params[:id])
    @jenis_info = @data_info_detail.jenis_info

    respond_to do |format|
      if @data_info_detail.update_attributes(data_info_detail_params)
        
        format.html { redirect_to(admin_jenis_info_path(@data_info_detail.jenis_info_id), :notice => 'DataInfoDetail was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @data_info_detail.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @data_info_detail = DataInfoDetail.find(params[:id])
    @data_info_detail.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.xml  { head :ok }
    end
  end

  def get_all
      @data_info_details = DataInfoDetail.where(:jenis_info_id => params[:jenis_info_id])

      respond_to do |format|
        format.js {
          render :json => @data_info_details.to_json(:except => [:updated_at]),
          :callback => params[:callback]
        }
      end
  end

  private

  def data_info_detail_params
    params.require(:data_info_detail).permit(:created_by, :jenis_info_id, :name, :updated_by)
  end
end
