class Admin::JenisInfosController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @jenis_infos = JenisInfo.order("name DESC")
  end
  
  def new
    @jenis_info = JenisInfo.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @jenis_info }
    end
  end
  
  def show
    @jenis_info = JenisInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @jenis_info }
    end
  end
  
  def create
    @jenis_info = JenisInfo.new(jenis_info_params)

    respond_to do |format|
      if @jenis_info.save
        
        format.html { redirect_to(admin_jenis_infos_path, :notice => 'jenis_info was successfully created.') }
        format.xml  { render :xml => @jenis_info, :status => :created, :jenis_info => @jenis_info }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @jenis_info.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @jenis_info = JenisInfo.find(params[:id])
  end
  
  def update
    @jenis_info = JenisInfo.find(params[:id])

    respond_to do |format|
      if @jenis_info.update_attributes(jenis_info_params)
        
        format.html { redirect_to(admin_jenis_infos_path, :notice => 'jenis_info was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @jenis_info.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @jenis_info = JenisInfo.find(params[:id])
    @jenis_info.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/jenis_infos') }
      format.xml  { head :ok }
    end
  end

  private

  def jenis_info_params
    params.require(:jenis_info).permit(:created_by, :name, :updated_by)
  end
end
