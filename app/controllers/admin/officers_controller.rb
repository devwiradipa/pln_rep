class Admin::OfficersController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @officers = Officer.order("name DESC")

    if current_admin.user_group && current_admin.user_group.super == 1
      @officers = Officer.order("name DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @officers = Officer.where("kawasan_id = ?", @kawasan.id).order("created_at DESC")
        @areas = Area.where(:id => @kawasan.area_id)
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
      else
        @officers = Officer.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
      if @rayon
        @officers = Officer.where("rayon_id = ?", @rayon.rayon_id).order("created_at DESC")
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
      else
        @officers = Officer.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @officers = Officer.where("area_id = ?", @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      else
        @officers = Officer.where("id IS NULL")
      end
    else
      @officers = Officer.where("id IS NULL")
    end
  end
  
  def new
    @officer = Officer.new
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @officer }
    end
  end
  
  def show
    @officer = Officer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @officer }
    end
  end
  
  def create
    @officer = Officer.new(officer_params)
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end

    respond_to do |format|
      if @officer.save
        
        format.html { redirect_to(admin_officers_path, :notice => 'officer was successfully created.') }
        format.xml  { render :xml => @officer, :status => :created, :officer => @officer }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @officer.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @officer = Officer.find(params[:id])
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end
  end
  
  def update
    @officer = Officer.find(params[:id])
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end

    respond_to do |format|
      if @officer.update_attributes(officer_params)
        
        format.html { redirect_to(admin_officers_path, :notice => 'officer was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @officer.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @officer = Officer.find(params[:id])
    @officer.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/officers') }
      format.xml  { head :ok }
    end
  end

  private

  def officer_params
    params.require(:officer).permit(:created_by, :name, :updated_by, :area_id, :rayon_id, :kawasan_id)
  end
end
