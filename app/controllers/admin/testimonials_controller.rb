class Admin::TestimonialsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @testimonials = Testimonial.order("created_at DESC")
  end
  
  def new
    @testimonial = Testimonial.new

    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @testimonial }
    end
  end
  
  def show
    @testimonial = Testimonial.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @testimonial }
    end
  end
  
  def create
    @testimonial = Testimonial.new(testimonial_params)
    @testimonial.featured = 0

    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      if @testimonial.save
        
        format.html { redirect_to(admin_testimonial_path(@testimonial), :notice => 'testimonial was successfully created.') }
        format.xml  { render :xml => @testimonial, :status => :created, :testimonial => @testimonial }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @testimonial.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @testimonial = Testimonial.find(params[:id])

    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
  end
  
  def update
    @testimonial = Testimonial.find(params[:id])

    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      if @testimonial.update_attributes(testimonial_params)
        
        format.html { redirect_to(admin_testimonial_path(@testimonial), :notice => 'testimonial was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @testimonial.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @testimonial = Testimonial.find(params[:id])
    @testimonial.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/testimonials') }
      format.xml  { head :ok }
    end
  end

  def featured
    @testimonial = Testimonial.find(params[:id])
    if @testimonial.featured == 1
      @testimonial.update_attributes(:featured => 0)
    else
      @testimonial.update_attributes(:featured => 1)
    end

    redirect_to :back
  end

  private

  def testimonial_params
    params.require(:testimonial).permit(:body, :created_by, :testimonial_by, :updated_by, :featured, :user_id)
  end
end
