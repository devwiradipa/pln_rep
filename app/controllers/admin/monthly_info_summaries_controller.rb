class Admin::MonthlyInfoSummariesController < ApplicationController
	before_filter :authenticate_admin!
  	before_filter :authorize_admin

	layout 'admin'
	  # GET /monthly_infos
	  # GET /monthly_infos.json
	def index
	    @jenis_layanans = JenisLayanan.all
	    if current_admin.user_group && current_admin.user_group.super == 1
	      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
	      @areas = Area.order("name ASC")
    	  @kawasans = Kawasan.order("nama_kawasan ASC")
    	  @rayons = Rayon.order("name ASC")
	    elsif current_admin.user_group && current_admin.kawasan
	      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
	      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
	      if @kawasan
	        @areas = Area.where("id = ?", @kawasan.area_id)
	        @areas = Area.where("id IS NULL")
	        @rayons = Rayon.where("id IS NULL")
	        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
	      else
	        @users = User.where("id IS NULL") 
	        @areas = Area.where("id IS NULL")
	        @rayons = Rayon.where("id IS NULL")
	        @kawasans = Kawasan.where("id IS NULL")
	      end
	    elsif current_admin.user_group && current_admin.rayon
	      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
	      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
	      if @rayon
	        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
	        @areas = Area.where("area_id = ?", @rayon.area_new_id)
	        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
	      else
	        @users = User.where("id IS NULL") 
	        @areas = Area.where("id IS NULL")
	        @rayons = Rayon.where("id IS NULL")
	        @kawasans = Kawasan.where("id IS NULL")
	      end
	    elsif current_admin.user_group && current_admin.area
	      @area = Area.where(:area_id => current_admin.area_new_id).first
	      @areas = Area.where(:area_id => current_admin.area_new_id)
	      if @area
	        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
	        @kawasans = @area.kawasans.order("nama_kawasan ASC")
	        @rayons = @area.rayons.order("name ASC")
	      else
	        @users = User.where("id IS NULL") 
	        @areas = Area.where("id IS NULL")
	        @rayons = Rayon.where("id IS NULL")
	        @kawasans = Kawasan.where("id IS NULL")
	      end
	    else
	      @users = User.where("id IS NULL") 
          @areas = Area.where("id IS NULL")
          @rayons = Rayon.where("id IS NULL")
          @kawasans = Kawasan.where("id IS NULL")
	    end

	    
		if params[:user_id] && params[:user_id] != ""
			@user = User.find(params[:user_id].to_i)
			@user_id = @user.id
		else
			@user_id = 0
		end

		if params[:area_id] && params[:area_id] != ""
			@area = Area.find(params[:area_id])
			@area_id = @area.id
		else
			@area_id = 0
		end

		if params[:rayon_id] && params[:rayon_id] != ""
			@rayon = Rayon.find(params[:rayon_id])
			@rayon_id = @rayon.id
		else
			@rayon_id = 0
		end

		if params[:kawasan_id] && params[:kawasan_id] != ""
			@kawasan = Kawasan.find(params[:kawasan_id])
			@kawasan_id = @kawasan.id
		else
			@kawasan_id = 0
		end

		if params[:blth_bulan_awal] && params[:blth_bulan_awal] != ""
			@blth_bulan_awal = params[:blth_bulan_awal].to_i
		else
			@blth_bulan_awal = Time.now.month-2
		end

		if params[:blth_bulan_akhir] && params[:blth_bulan_akhir] != ""
			@blth_bulan_akhir = params[:blth_bulan_akhir].to_i
		else
			@blth_bulan_akhir = Time.now.month
		end

		if params[:blth_tahun_awal] && params[:blth_tahun_awal] != ""
			@blth_tahun_awal = params[:blth_tahun_awal].to_i
		else
			@blth_tahun_awal = Time.now.year
		end

		if params[:blth_tahun_akhir] && params[:blth_tahun_akhir] != ""
			@blth_tahun_akhir = params[:blth_tahun_akhir].to_i
		else
			@blth_tahun_akhir = Time.now.year
		end

		if @blth_bulan_awal < 0
			@blth_bulan_awal = @blth_bulan_awal+12
			@blth_tahun_awal = @blth_tahun_awal-1
		end

		if @user
			@monthly_infos = MonthlyInfo.where(
				"user_id = ? AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@user_id.to_i, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					order("monthly_infos.tahun_blth ASC, monthly_infos.bulan_blth ASC")
		elsif @kawasan
			@jenis_layanans = JenisLayanan.all
			@premium_users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
			@monthly_infos = MonthlyInfo.select("sum(durasi_gangguan) as durasi_gangguan, sum(frekuensi_gangguan) as frekuensi_gangguan, sum(kwh_lwbp) as kwh_lwbp, sum(kwh_wbp) as kwh_wbp, sum(kwh_total) as kwh_total, sum(kwh_kvarh) as kwh_kvarh, sum(rp_kvarh) as rp_kvarh, sum(rp_lwbp) as rp_lwbp, sum(rp_wbp) as rp_wbp, sum(rptag) as rptag, bulan_blth, tahun_blth, kvamaks, beban_min, beban_maks, beban_rata").
				where.("user_id IN (?) AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@premium_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					group("monthly_infos.tahun_blth, monthly_infos.bulan_blth")
		elsif @rayon
			@jenis_layanans = JenisLayanan.all
			@premium_users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
			@monthly_infos = MonthlyInfo.select("sum(durasi_gangguan) as durasi_gangguan, sum(frekuensi_gangguan) as frekuensi_gangguan, sum(kwh_lwbp) as kwh_lwbp, sum(kwh_wbp) as kwh_wbp, sum(kwh_total) as kwh_total, sum(kwh_kvarh) as kwh_kvarh, sum(rp_kvarh) as rp_kvarh, sum(rp_lwbp) as rp_lwbp, sum(rp_wbp) as rp_wbp, sum(rptag) as rptag, bulan_blth, tahun_blth, kvamaks, beban_min, beban_maks, beban_rata"),
				where("user_id IN (?) AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@premium_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					group("monthly_infos.tahun_blth, monthly_infos.bulan_blth")
		elsif @area
			@jenis_layanans = JenisLayanan.all
			@premium_users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
			@monthly_infos = MonthlyInfo.select("sum(durasi_gangguan) as durasi_gangguan, sum(frekuensi_gangguan) as frekuensi_gangguan, sum(kwh_lwbp) as kwh_lwbp, sum(kwh_wbp) as kwh_wbp, sum(kwh_total) as kwh_total, sum(kwh_kvarh) as kwh_kvarh, sum(rp_kvarh) as rp_kvarh, sum(rp_lwbp) as rp_lwbp, sum(rp_wbp) as rp_wbp, sum(rptag) as rptag, bulan_blth, tahun_blth, kvamaks, beban_min, beban_maks, beban_rata"),
				where("user_id IN (?) AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@premium_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					group("monthly_infos.tahun_blth, monthly_infos.bulan_blth" )
		else
			@monthly_infos = MonthlyInfo.
				where("user_id = ? AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@user_id.to_i, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					order("monthly_infos.tahun_blth ASC, monthly_infos.bulan_blth ASC" )
		end
  
  		if @user
          username = @user.name
        elsif @kawasan
          username = @kawasan.nama_kawasan
        elsif @rayon
          username = @rayon.name
        elsif @area
          username = @area.name
      	else
      	  username = ""
        end

		filename = "Summary Data Bulanan - "+username+".xls" 
		respond_to do |format|
		  format.html # index.html.erb
		  format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
		end
	end

	def get_data
		@jenis_layanans = JenisLayanan.all
	    if current_admin.user_group && current_admin.user_group.super == 1
	      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
	      @areas = Area.order("name ASC")
    	  @kawasans = Kawasan.order("nama_kawasan ASC")
    	  @rayons = Rayon.order("name ASC")
	    elsif current_admin.user_group && current_admin.kawasan
	      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
	      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
	      if @kawasan
	        @areas = Area.where("id = ?", @kawasan.area_id)
	        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
	      else
	        @users = User.where("id IS NULL") 
	      end
	    elsif current_admin.user_group && current_admin.rayon
	      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
	      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
	      if @rayon
	        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
	        @areas = Area.where("area_id = ?", @rayon.area_new_id)
	        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
	      else
	        @users = User.where("id IS NULL")
	      end
	    elsif current_admin.user_group && current_admin.area
	      @area = Area.where(:area_id => current_admin.area_new_id).first
	      @areas = Area.where(:area_id => current_admin.area_new_id)
	      if @area
	        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
	        @kawasans = @area.kawasans.order("nama_kawasan ASC")
	      else
	        @users = User.where("id IS NULL")
	      end
	    else
	      @users = User.where("id IS NULL")
	    end

	    
		if params[:user_id] && params[:user_id] != ""
			@user = User.find(params[:user_id].to_i)
			@user_id = @user.id
		else
			@user_id = 0
		end

		if params[:area_id] && params[:area_id] != ""
			@area = Area.find(params[:area_id])
			@area_id = @area.id
		else
			@area_id = 0
		end

		if params[:rayon_id] && params[:rayon_id] != ""
			@rayon = Rayon.find(params[:rayon_id])
			@rayon_id = @rayon.id
		else
			@rayon_id = 0
		end

		if params[:kawasan_id] && params[:kawasan_id] != ""
			@kawasan = Kawasan.find(params[:kawasan_id])
			@kawasan_id = @kawasan.id
		else
			@kawasan_id = 0
		end

		if params[:blth_bulan_awal] && params[:blth_bulan_awal] != ""
			@blth_bulan_awal = params[:blth_bulan_awal].to_i
		else
			@blth_bulan_awal = Time.now.month-2
		end

		if params[:blth_bulan_akhir] && params[:blth_bulan_akhir] != ""
			@blth_bulan_akhir = params[:blth_bulan_akhir].to_i
		else
			@blth_bulan_akhir = Time.now.month
		end

		if params[:blth_tahun_awal] && params[:blth_tahun_awal] != ""
			@blth_tahun_awal = params[:blth_tahun_awal].to_i
		else
			@blth_tahun_awal = Time.now.year
		end

		if params[:blth_tahun_akhir] && params[:blth_tahun_akhir] != ""
			@blth_tahun_akhir = params[:blth_tahun_akhir].to_i
		else
			@blth_tahun_akhir = Time.now.year
		end

		if @blth_bulan_awal < 0
			@blth_bulan_awal = @blth_bulan_awal+12
			@blth_tahun_awal = @blth_tahun_awal-1
		end

		if @user
			@monthly_infos = MonthlyInfo. 
				where("user_id = ? AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@user_id.to_i, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					order("monthly_infos.tahun_blth ASC, monthly_infos.bulan_blth ASC" )
		elsif @kawasan
			@jenis_layanans = JenisLayanan.all
			@premium_users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
			@monthly_infos = MonthlyInfo.select("sum(durasi_gangguan) as durasi_gangguan, sum(frekuensi_gangguan) as frekuensi_gangguan, sum(kwh_lwbp) as kwh_lwbp, sum(kwh_wbp) as kwh_wbp, sum(kwh_total) as kwh_total, sum(kwh_kvarh) as kwh_kvarh, sum(rp_kvarh) as rp_kvarh, sum(rp_lwbp) as rp_lwbp, sum(rp_wbp) as rp_wbp, sum(rptag) as rptag, bulan_blth, tahun_blth").
				where("user_id IN (?) AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@premium_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					group("monthly_infos.tahun_blth, monthly_infos.bulan_blth" )
		elsif @rayon
			@jenis_layanans = JenisLayanan.all
			@premium_users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
			@monthly_infos = MonthlyInfo.select("sum(durasi_gangguan) as durasi_gangguan, sum(frekuensi_gangguan) as frekuensi_gangguan, sum(kwh_lwbp) as kwh_lwbp, sum(kwh_wbp) as kwh_wbp, sum(kwh_total) as kwh_total, sum(kwh_kvarh) as kwh_kvarh, sum(rp_kvarh) as rp_kvarh, sum(rp_lwbp) as rp_lwbp, sum(rp_wbp) as rp_wbp, sum(rptag) as rptag, bulan_blth, tahun_blth").
				where("user_id IN (?) AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@premium_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					group("monthly_infos.tahun_blth, monthly_infos.bulan_blth" )
		elsif @area
			@jenis_layanans = JenisLayanan.all
			@premium_users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
			@monthly_infos = MonthlyInfo.select("sum(durasi_gangguan) as durasi_gangguan, sum(frekuensi_gangguan) as frekuensi_gangguan, sum(kwh_lwbp) as kwh_lwbp, sum(kwh_wbp) as kwh_wbp, sum(kwh_total) as kwh_total, sum(kwh_kvarh) as kwh_kvarh, sum(rp_kvarh) as rp_kvarh, sum(rp_lwbp) as rp_lwbp, sum(rp_wbp) as rp_wbp, sum(rptag) as rptag, bulan_blth, tahun_blth").
				where("user_id IN (?) AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@premium_users.map(&:id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					group("monthly_infos.tahun_blth, monthly_infos.bulan_blth")
		else
			@monthly_infos = MonthlyInfo.
				where("user_id = ? AND ((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) 
					AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?))", 
					@user_id.to_i, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).
					order("monthly_infos.tahun_blth ASC, monthly_infos.bulan_blth ASC")
		end

		json_data = Hash.new

		summary_blth = []
		summary_durasi_gangguan = []
		summary_frekuensi_gangguan = []
		summary_kwh_kvarh = []
		summary_kwh_lwbp = []
		summary_kwh_wbp = []
		summary_kwh_total = []
		summary_rp_kvarh = []
		summary_rp_lwbp = []
		summary_rp_wbp = []
		summary_rp_total = []

		@monthly_infos.each do |monthly_info|
			summary_blth << @months[monthly_info.bulan_blth]+" "+monthly_info.tahun_blth.to_s
			summary_durasi_gangguan << monthly_info.durasi_gangguan
			summary_frekuensi_gangguan << monthly_info.frekuensi_gangguan
			summary_kwh_kvarh << monthly_info.kwh_kvarh
			summary_kwh_lwbp << monthly_info.kwh_lwbp
			summary_kwh_wbp << monthly_info.kwh_wbp
			summary_kwh_total << monthly_info.kwh_total
			summary_rp_kvarh << monthly_info.rp_kvarh
			summary_rp_lwbp << monthly_info.rp_lwbp
			summary_rp_wbp << monthly_info.rp_wbp
			summary_rp_total << monthly_info.rptag
		end

		json_data[:summary_blth] = summary_blth
		json_data[:summary_durasi_gangguan] = summary_durasi_gangguan
		json_data[:summary_frekuensi_gangguan] = summary_frekuensi_gangguan
		json_data[:summary_kwh_kvarh] = summary_kwh_kvarh
		json_data[:summary_kwh_lwbp] = summary_kwh_lwbp
		json_data[:summary_kwh_wbp] = summary_kwh_wbp
		json_data[:summary_kwh_total] = summary_kwh_total
		json_data[:summary_rp_kvarh] = summary_rp_kvarh
		json_data[:summary_rp_lwbp] = summary_rp_lwbp
		json_data[:summary_rp_wbp] = summary_rp_wbp
		json_data[:summary_rp_total] = summary_rp_total

		render :json => json_data, :layout => false
	end
end
