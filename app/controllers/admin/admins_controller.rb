class Admin::AdminsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @admins = Admin.order("created_at DESC")
  end
  
  def new
    @admin = Admin.new
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @admin }
    end
  end
  
  def show
    @admin = Admin.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @admin }
    end
  end
  
  def create
    @admin = Admin.new(admin_params)
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")

    respond_to do |format|
      if @admin.save
        
        format.html { redirect_to(admin_admin_path(@admin), :notice => 'admin was successfully created.') }
        format.xml  { render :xml => @admin, :status => :created, :admin => @admin }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @admin.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @admin = Admin.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
  end
  
  def update
    @admin = Admin.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")

    respond_to do |format|
      if @admin.update_attributes(admin_params)
        
        format.html { redirect_to(admin_admin_path(@admin), :notice => 'admin was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @admin.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @admin = Admin.find(params[:id])
    @admin.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/admins') }
      format.xml  { head :ok }
    end
  end

  def get_all
      @admins = Admin.where(:user_group_id => params[:user_group_id])

      respond_to do |format|
        format.js {
          render :json => @admins.to_json(:except => [:updated_at]),
          :callback => params[:callback]
        }
      end
  end

  private

  def admin_params
    params.require(:admin).permit(:email, :password, :password_confirmation, :remember_me, :username, :name, :user_group_id, :rayon_id, :area_id, :rayon_new_id, :area_new_id, :kawasan_id, :avatar)
  end
end
