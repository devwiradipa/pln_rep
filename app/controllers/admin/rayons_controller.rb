class Admin::RayonsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @rayons = Rayon.order("name DESC")
  end
  
  def new
    @rayon = Rayon.new
    @areas = Area.order("name ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @rayon }
    end
  end
  
  def show
    @rayon = Rayon.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @rayon }
    end
  end
  
  def create
    @rayon = Rayon.new(rayon_params)
    @areas = Area.order("name ASC")

    respond_to do |format|
      if @rayon.save
        
        format.html { redirect_to(admin_rayons_path, :notice => 'rayon was successfully created.') }
        format.xml  { render :xml => @rayon, :status => :created, :rayon => @rayon }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @rayon.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @rayon = Rayon.find(params[:id])
    @areas = Area.order("name ASC")
  end
  
  def update
    @rayon = Rayon.find(params[:id])
    @areas = Area.order("name ASC")

    respond_to do |format|
      if @rayon.update_attributes(rayon_params)
        
        format.html { redirect_to(admin_rayons_path, :notice => 'rayon was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @rayon.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @rayon = Rayon.find(params[:id])
    @rayon.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/rayons') }
      format.xml  { head :ok }
    end
  end

  def get_all
      @rayons = Rayon.where(:area_new_id => params[:area_id])

      respond_to do |format|
        format.js {
          render :json => @rayons.to_json(:except => [:updated_at]),
          :callback => params[:callback]
        }
      end
  end

  private

  def rayon_params
    params.require(:rayon).permit(:area_id, :created_by, :name, :updated_by, :area_new_id, :telp, :alamat, :fax,
  :pic_name_1, :pic_jabatan_1, :pic_hp_1, :pic_name_2, :pic_jabatan_2, :pic_hp_2, 
  :pic_name_3, :pic_jabatan_3, :pic_hp_3, :pic_name_4, :pic_jabatan_4, :pic_hp_4, :rayon_id)
  end
end
