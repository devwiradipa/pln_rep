class Admin::DayasController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @dayas = Daya.order("name DESC")
  end
  
  def new
    @daya = Daya.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @daya }
    end
  end
  
  def show
    @daya = Daya.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @daya }
    end
  end
  
  def create
    @daya = Daya.new(daya_params)

    respond_to do |format|
      if @daya.save
        
        format.html { redirect_to(admin_dayas_path, :notice => 'daya was successfully created.') }
        format.xml  { render :xml => @daya, :status => :created, :daya => @daya }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @daya.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @daya = Daya.find(params[:id])
  end
  
  def update
    @daya = Daya.find(params[:id])

    respond_to do |format|
      if @daya.update_attributes(daya_params)
        
        format.html { redirect_to(admin_dayas_path, :notice => 'daya was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @daya.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @daya = Daya.find(params[:id])
    @daya.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/dayas') }
      format.xml  { head :ok }
    end
  end

  private

  def daya_params
    params.require(:daya).permit(:description, :name)
  end
end
