class Admin::MasterTarifsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @master_tarifs = MasterTarif.order("name DESC")
  end
  
  def new
    @master_tarif = MasterTarif.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @master_tarif }
    end
  end
  
  def show
    @master_tarif = MasterTarif.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @master_tarif }
    end
  end
  
  def create
    @master_tarif = MasterTarif.new(master_tarif_params)

    respond_to do |format|
      if @master_tarif.save
        
        format.html { redirect_to(admin_master_tarifs_path, :notice => 'master_tarif was successfully created.') }
        format.xml  { render :xml => @master_tarif, :status => :created, :master_tarif => @master_tarif }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @master_tarif.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @master_tarif = MasterTarif.find(params[:id])
  end
  
  def update
    @master_tarif = MasterTarif.find(params[:id])

    respond_to do |format|
      if @master_tarif.update_attributes(master_tarif_params)
        
        format.html { redirect_to(admin_master_tarifs_path, :notice => 'master_tarif was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @master_tarif.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @master_tarif = MasterTarif.find(params[:id])
    @master_tarif.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/master_tarifs') }
      format.xml  { head :ok }
    end
  end

  private

  def master_tarif_params
    params.require(:master_tarif).permit(:description, :name)
  end
end
