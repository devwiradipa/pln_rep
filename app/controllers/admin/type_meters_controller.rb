class Admin::TypeMetersController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @type_meters = TypeMeter.order("name DESC")
  end
  
  def new
    @type_meter = TypeMeter.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @type_meter }
    end
  end
  
  def show
    @type_meter = TypeMeter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @type_meter }
    end
  end
  
  def create
    @type_meter = TypeMeter.new(type_meter_params)

    respond_to do |format|
      if @type_meter.save
        
        format.html { redirect_to(admin_type_meters_path, :notice => 'type_meter was successfully created.') }
        format.xml  { render :xml => @type_meter, :status => :created, :type_meter => @type_meter }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @type_meter.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @type_meter = TypeMeter.find(params[:id])
  end
  
  def update
    @type_meter = TypeMeter.find(params[:id])

    respond_to do |format|
      if @type_meter.update_attributes(type_meter_params)
        
        format.html { redirect_to(admin_type_meters_path, :notice => 'type_meter was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @type_meter.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @type_meter = TypeMeter.find(params[:id])
    @type_meter.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/type_meters') }
      format.xml  { head :ok }
    end
  end

  private

  def type_meter_params
    params.require(:type_meter).permit(:created_by, :description, :name, :updated_by)
  end
end
