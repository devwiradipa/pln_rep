class Admin::ArticlesController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @articles = Article.order("created_at DESC")
  end
  
  def new
    @article = Article.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @article }
    end
  end
  
  def show
    @article = Article.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @article }
    end
  end
  
  def create
    @article = Article.new(article_params)
    @article.featured_status = 0
    @article.publish_status = 1

    respond_to do |format|
      if @article.save
        
        format.html { redirect_to(admin_article_path(@article), :notice => 'article was successfully created.') }
        format.xml  { render :xml => @article, :status => :created, :article => @article }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
    @article = Article.find(params[:id])

    respond_to do |format|
      if @article.update_attributes(article_params)
        
        format.html { redirect_to(admin_article_path(@article), :notice => 'article was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/articles') }
      format.xml  { head :ok }
    end
  end

  def featured
    @article = Article.find(params[:id])
    if @article.featured_status == 1
      @article.update_attributes(:featured_status => 0)
    else
      @article.update_attributes(:featured_status => 1)
    end

    redirect_to :back
  end

  def publish
    @article = Article.find(params[:id])
    if @article.publish_status == 1
      @article.update_attributes(:publish_status => 0)
    else
      @article.update_attributes(:publish_status => 1)
    end

    redirect_to :back
  end

  private

  def article_params
    params.require(:article).permit(:body, :created_by, :featured_status, :publish_status, :title, :updated_by, :image, :summary)
  end
end
