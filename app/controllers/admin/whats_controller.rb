class Admin::WhatsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @body_content = BodyContent.where("title = 'what'").first

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @body_content }
    end
  end
  
  def edit
    @body_content = BodyContent.find(params[:id])
  end
  
  def update
    @body_content = BodyContent.find(params[:id])

    respond_to do |format|
      if @body_content.update_attributes(what_params)
        
        format.html { redirect_to(admin_whats_path, :notice => 'what was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @body_content.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

  def what_params
    params.require(:body_content).permit(:body, :created_by, :title, :updated_by)
  end
end
