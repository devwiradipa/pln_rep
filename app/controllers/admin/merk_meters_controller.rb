class Admin::MerkMetersController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @merk_meters = MerkMeter.order("name DESC")
  end
  
  def new
    @merk_meter = MerkMeter.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @merk_meter }
    end
  end
  
  def show
    @merk_meter = MerkMeter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @merk_meter }
    end
  end
  
  def create
    @merk_meter = MerkMeter.new(merk_meter_params)

    respond_to do |format|
      if @merk_meter.save
        
        format.html { redirect_to(admin_merk_meters_path, :notice => 'merk_meter was successfully created.') }
        format.xml  { render :xml => @merk_meter, :status => :created, :merk_meter => @merk_meter }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @merk_meter.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @merk_meter = MerkMeter.find(params[:id])
  end
  
  def update
    @merk_meter = MerkMeter.find(params[:id])

    respond_to do |format|
      if @merk_meter.update_attributes(merk_meter_params)
        
        format.html { redirect_to(admin_merk_meters_path, :notice => 'merk_meter was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @merk_meter.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @merk_meter = MerkMeter.find(params[:id])
    @merk_meter.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/merk_meters') }
      format.xml  { head :ok }
    end
  end

  private

  def merk_meter_params
    params.require(:merk_meter).permit(:created_by, :description, :name, :updated_by, :pabrikan_meter)
  end
end
