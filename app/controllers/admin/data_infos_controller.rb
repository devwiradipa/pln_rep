class Admin::DataInfosController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index

    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @data_infos = DataInfo.order("tgl_info_gangguan DESC")
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)

      if @kawasan
        @areas = Area.where("id = ?", @kawasan.area_id)
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
        @kawasans_data_infos = KawasansDataInfo.where("kawasan_id = ?", @kawasan.id)
        @users_data_infos = UsersDataInfo.where("user_id IN (?)", @users.map(&:id))
        @data_infos = DataInfo.where("id IN (?) OR id IN (?)", @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id)).order("tgl_info_gangguan DESC")
      else
        @data_infos = DataInfo.order("tgl_info_gangguan DESC")
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
        @users_data_infos = UsersDataInfo.where("user_id IN (?)", @users.map(&:id))
        @data_infos = DataInfo.where("id IN (?)", @users_data_infos.map(&:data_info_id)).order("tgl_info_gangguan DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @areas_data_infos = AreasDataInfo.where("area_id = ?", @area.id)
        @kawasans_data_infos = KawasansDataInfo.where("kawasan_id IN (?)", @kawasans.map(&:id))
        @users_data_infos = UsersDataInfo.where("user_id IN (?)", @users.map(&:id))
        @data_infos = DataInfo.where("id IN (?) OR id IN (?) OR id IN (?)", @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id), @areas_data_infos.map(&:data_info_id)).order("tgl_info_gangguan DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    if params[:area_id] && params[:area_id] != ""
      @areas_data_infos = AreasDataInfo.where("area_id = ?", params[:area_id].to_i)
    else
      @areas_data_infos = AreasDataInfo.where("area_id IS NULL")
    end

    if params[:kawasan_id] && params[:kawasan_id] != ""
      @kawasans_data_infos = KawasansDataInfo.where("kawasan_id = ?", params[:kawasan_id].to_i)
    else
      @kawasans_data_infos = KawasansDataInfo.where("kawasan_id IS NULL")
    end

    if params[:user_id] && params[:user_id] != ""
      @users_data_infos = UsersDataInfo.where("user_id = ?", params[:user_id].to_i)
    else
      @users_data_infos = UsersDataInfo.where("user_id IS NULL")
    end

    if params[:blth_bulan_awal] && params[:blth_bulan_awal] != ""
      @blth_bulan_awal = params[:blth_bulan_awal].to_i
    else
      @blth_bulan_awal = 1
    end

    if params[:blth_bulan_akhir] && params[:blth_bulan_akhir] != ""
      @blth_bulan_akhir = params[:blth_bulan_akhir].to_i
    else
      @blth_bulan_akhir = 12
    end

    if params[:blth_tahun_awal] && params[:blth_tahun_awal] != ""
      @blth_tahun_awal = params[:blth_tahun_awal].to_i
    else
      @blth_tahun_awal = 1970
    end

    if params[:blth_tahun_akhir] && params[:blth_tahun_akhir] != ""
      @blth_tahun_akhir = params[:blth_tahun_akhir].to_i
    else
      @blth_tahun_akhir = 3000
    end

    if (! params[:area_id] || params[:area_id] == "") && (! params[:kawasan_id] || params[:kawasan_id] == "") && (! params[:user_id] || params[:user_id] == "")

    else
      @data_infos = DataInfo.where("id IN (?) AND (id IN (?) OR id IN (?) OR id IN (?)) AND ((MONTH(data_infos.tgl_info_gangguan) >= ? AND YEAR(data_infos.tgl_info_gangguan) >= ?) OR (MONTH(data_infos.tgl_info_gangguan) < ? AND YEAR(data_infos.tgl_info_gangguan) > ?)) 
        AND ((MONTH(data_infos.tgl_info_gangguan) <= ? AND YEAR(data_infos.tgl_info_gangguan) <= ?) OR (MONTH(data_infos.tgl_info_gangguan) > ? AND YEAR(data_infos.tgl_info_gangguan < ?)))", @data_infos.map(&:id), @areas_data_infos.map(&:data_info_id), @kawasans_data_infos.map(&:data_info_id), @users_data_infos.map(&:data_info_id), @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir).order("tgl_info_gangguan DESC")
    end

  end
  
  def new
    @data_info = DataInfo.new
    @jenis_infos = JenisInfo.all
    @data_info_details = DataInfoDetail.all
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @areas = Area.where("id = ?", @kawasan.area_id)
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_info }
    end
  end
  
  def show
    @data_info = DataInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_info }
    end
  end

  
  def create
    @data_info = DataInfo.new(data_info_params)
    @jenis_infos = JenisInfo.all
    @data_info_details = DataInfoDetail.all
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @areas = Area.where("id = ?", @kawasan.area_id)
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      if @data_info.save

        if @data_info.time_end && @data_info.time_start
          @data_info.update_attributes(:duration => ((@data_info.time_end-@data_info.time_start)/3600).to_f)
        end

        User.where("jenis_layanan_id IN (?) AND (area_new_id IN (?) OR kawasan_id IN (?) OR id IN (?))", @jenis_layanans.map(&:id), @data_info.areas.map(&:area_id), @data_info.kawasans.map(&:id), @data_info.users.map(&:id)).each do |user|
          # UserMailer.data_info_notification(@data_info,user).deliver
          # sidekiq can only use non object parameter
          info_gangguan = @data_info.info_gangguan
          tgl_info_gangguan = @data_info.tgl_info_gangguan
          id = @data_info.id.to_s
          if @data_info.created
            if @data_info.created.kawasan
              created_by = "Pengelola Kawasan "+ @data_info.created.kawasan.nama_kawasan
            elsif @data_info.created.rayon
              created_by = "PLN Rayon "+ @data_info.created.rayon.name
            elsif @data_info.created.area
              created_by = "PLN Area "+ @data_info.created.area.name
            else
              created_by = "PLN Kantor Distribusi"
            end
          end

          receiver_name = user.name
          email = user.email
          UserMailer.data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, @hostname).deliver
        end

        @emails = @data_info.list_email.split(";")
        @emails.each do |email|
          # UserMailer.data_info_email_notification(@data_info,email).deliver
          # sidekiq can only use non object parameter
          info_gangguan = @data_info.info_gangguan
          tgl_info_gangguan = @data_info.tgl_info_gangguan
          id = @data_info.id.to_s
          receiver_name = email
          email = email

          if @data_info.created
            if @data_info.created.kawasan
              created_by = "Pengelola Kawasan "+ @data_info.created.kawasan.nama_kawasan
            elsif @data_info.created.rayon
              created_by = "PLN Rayon "+ @data_info.created.rayon.name
            elsif @data_info.created.area
              created_by = "PLN Area "+ @data_info.created.area.name
            else
              created_by = "PLN Kantor Distribusi"
            end
          end

          UserMailer.data_info_email_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, @hostname).deliver
        end

        @data_info.kawasans.each do |kawasan|
          kawasan.admins.each do |admin|
            if admin.email
              info_gangguan = @data_info.info_gangguan
              tgl_info_gangguan = @data_info.tgl_info_gangguan
              id = @data_info.id.to_s
              receiver_name = admin.email
              email = admin.email

              if @data_info.created
                if @data_info.created.kawasan
                  created_by = "Pengelola Kawasan "+ @data_info.created.kawasan.nama_kawasan
                elsif @data_info.created.rayon
                  created_by = "PLN Rayon "+ @data_info.created.rayon.name
                elsif @data_info.created.area
                  created_by = "PLN Area "+ @data_info.created.area.name
                else
                  created_by = "PLN Kantor Distribusi"
                end
              end

              UserMailer.admin_data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, @hostname).deliver
            end
          end

          if kawasan.area
            kawasan.area.admins.each do |admin|
              if admin.email
                info_gangguan = @data_info.info_gangguan
                tgl_info_gangguan = @data_info.tgl_info_gangguan
                id = @data_info.id.to_s
                receiver_name = admin.name
                email = admin.email

                if @data_info.created
                  if @data_info.created.kawasan
                    created_by = "Pengelola Kawasan "+ @data_info.created.kawasan.nama_kawasan
                  elsif @data_info.created.rayon
                    created_by = "PLN Rayon "+ @data_info.created.rayon.name
                  elsif @data_info.created.area
                    created_by = "PLN Area "+ @data_info.created.area.name
                  else
                    created_by = "PLN Kantor Distribusi"
                  end
                end

                UserMailer.admin_data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, @hostname).deliver
              end
            end
          end
        end

        @data_info.areas.each do |area|
          area.admins.each do |admin|
            if admin.email
              info_gangguan = @data_info.info_gangguan
              tgl_info_gangguan = @data_info.tgl_info_gangguan
              id = @data_info.id.to_s
              receiver_name = admin.name
              email = admin.email

              if @data_info.created
                if @data_info.created.kawasan
                  created_by = "Pengelola Kawasan "+ @data_info.created.kawasan.nama_kawasan
                elsif @data_info.created.rayon
                  created_by = "PLN Rayon "+ @data_info.created.rayon.name
                elsif @data_info.created.area
                  created_by = "PLN Area "+ @data_info.created.area.name
                else
                  created_by = "PLN Kantor Distribusi"
                end
              end

              UserMailer.admin_data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, @hostname).deliver
            end
          end
        end

        @super = UserGroup.where("super = 1")
        @admin_kds = Admin.where("user_group_id IN (?)", @super.map(&:id))

        @admin_kds.each do |admin|
          if admin.email
            info_gangguan = @data_info.info_gangguan
            tgl_info_gangguan = @data_info.tgl_info_gangguan
            id = @data_info.id.to_s
            receiver_name = admin.name
            email = admin.email

            if @data_info.created
              if @data_info.created.kawasan
                created_by = "Pengelola Kawasan "+ @data_info.created.kawasan.nama_kawasan
              elsif @data_info.created.rayon
                created_by = "PLN Rayon "+ @data_info.created.rayon.name
              elsif @data_info.created.area
                created_by = "PLN Area "+ @data_info.created.area.name
              else
                created_by = "PLN Kantor Distribusi"
              end
            end

            UserMailer.admin_data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, @hostname).deliver
          end
        end
        
        format.html { redirect_to(admin_data_info_path(@data_info), :notice => 'data_info was successfully created.') }
        format.xml  { render :xml => @data_info, :status => :created, :data_info => @data_info }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_info.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @data_info = DataInfo.find(params[:id])
    @jenis_infos = JenisInfo.all
    @data_info_details = DataInfoDetail.all
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @areas = Area.where("id = ?", @kawasan.area_id)
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end
  end
  
  def update
    @data_info = DataInfo.find(params[:id])
    @jenis_infos = JenisInfo.all
    @data_info_details = DataInfoDetail.all
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @areas = Area.where("id = ?", @kawasan.area_id)
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    respond_to do |format|
      if @data_info.update_attributes(data_info_params)

        @data_info.update_attributes(:duration => ((@data_info.time_end-@data_info.time_start)/3600).to_f)
        
        format.html { redirect_to(admin_data_info_path(@data_info), :notice => 'data_info was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @data_info.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @data_info = DataInfo.find(params[:id])
    KawasansDataInfo.delete_all(:data_info_id => @data_info)
    AreasDataInfo.delete_all(:data_info_id => @data_info)
    UsersDataInfo.delete_all(:data_info_id => @data_info)
    @data_info.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/data_infos') }
      format.xml  { head :ok }
    end
  end

  def get_all
      @data_infos = DataInfo.where(:jenis_info_id => params[:jenis_info_id])

      respond_to do |format|
        format.js {
          render :json => @data_infos.to_json(:except => [:updated_at]),
          :callback => params[:callback]
        }
      end
  end

  private

  def data_info_params
    params.require(:data_info).permit(:idpel, :info_dari_pln, :info_gangguan, :info_lainnya, :info_ren_pemeliharaan, :tgl_dari_pln, 
  :tgl_info_gangguan, :tgl_info_lainnya, :tgl_ren_pemeliharaan, :jenis_info_id, :data_info_detail_id, :file, 
  :area_ids, :kawasan_ids, :user_ids, :list_email, :time_start, :time_end, :duration, :created_by)
  end
end
