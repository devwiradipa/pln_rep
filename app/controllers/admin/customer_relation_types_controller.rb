class Admin::CustomerRelationTypesController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @customer_relation_types = CustomerRelationType.order("name DESC")
  end
  
  def new
    @customer_relation_type = CustomerRelationType.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @customer_relation_type }
    end
  end
  
  def show
    @customer_relation_type = CustomerRelationType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @customer_relation_type }
    end
  end
  
  def create
    @customer_relation_type = CustomerRelationType.new(customer_relation_type_params)

    respond_to do |format|
      if @customer_relation_type.save
        
        format.html { redirect_to(admin_customer_relation_types_path, :notice => 'customer_relation_type was successfully created.') }
        format.xml  { render :xml => @customer_relation_type, :status => :created, :customer_relation_type => @customer_relation_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @customer_relation_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @customer_relation_type = CustomerRelationType.find(params[:id])
  end
  
  def update
    @customer_relation_type = CustomerRelationType.find(params[:id])

    respond_to do |format|
      if @customer_relation_type.update_attributes(customer_relation_type_params)
        
        format.html { redirect_to(admin_customer_relation_types_path, :notice => 'customer_relation_type was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @customer_relation_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @customer_relation_type = CustomerRelationType.find(params[:id])
    @customer_relation_type.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/customer_relation_types') }
      format.xml  { head :ok }
    end
  end

  private

  def customer_relation_type_params
    params.require(:customer_relation_type).permit(:created_by, :name, :updated_by)
  end
end
