class Admin::UserGroupsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @user_groups = UserGroup.order("name DESC")
  end
  
  def new
    @user_group = UserGroup.new
    @controller_models = ControllerModel.where("position = 'BackEnd'")
    @action_models = ActionModel.where("controller_model_id IN (?)", @controller_models.map(&:id)).order("controller_model_id ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user_group }
    end
  end
  
  def show
    @user_group = UserGroup.find(params[:id])
    @action_models = @user_group.action_models.order("controller_model_id ASC")

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user_group }
    end
  end
  
  def create
    @user_group = UserGroup.new(user_group_params)
    @controller_models = ControllerModel.where("position = 'BackEnd'")
    @action_models = ActionModel.where("controller_model_id IN (?)", @controller_models.map(&:id)).order("controller_model_id ASC")

    respond_to do |format|
      if @user_group.save
        
        format.html { redirect_to(admin_user_group_path(@user_group.id), :notice => 'user_group was successfully created.') }
        format.xml  { render :xml => @user_group, :status => :created, :user_group => @user_group }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user_group.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @user_group = UserGroup.find(params[:id])
    @controller_models = ControllerModel.where("position = 'BackEnd'")
    @action_models = ActionModel.where("controller_model_id IN (?)", @controller_models.map(&:id)).order("controller_model_id ASC")
  end
  
  def update
    @user_group = UserGroup.find(params[:id])
    @controller_models = ControllerModel.where("position = 'BackEnd'")
    @action_models = ActionModel.where("controller_model_id IN (?)", @controller_models.map(&:id)).order("controller_model_id ASC")

    respond_to do |format|
      if @user_group.update_attributes(user_group_params)
        
        format.html { redirect_to(admin_user_group_path(@user_group), :notice => 'user_group was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user_group.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user_group = UserGroup.find(params[:id])
    @user_group.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/user_groups') }
      format.xml  { head :ok }
    end
  end

  def update_status
    @user_group = UserGroup.find(params[:id])
    if @user_group.super == 1
      @user_group.update_attributes(:super => 0)
    else
      @user_group.update_attributes(:super => 1)
    end

    redirect_to :back
  end

  private

  def user_group_params
    params.require(:user_group).permit(:created_by, :description, :name, :updated_by, :action_model_ids, :active, :deleted_by, :super)
  end
end
