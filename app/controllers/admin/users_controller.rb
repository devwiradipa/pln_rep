class Admin::UsersController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin

  layout 'admin'
  # GET /users
  # GET /users.json
  def index
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id != 1")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
        @areas = Area.where(:id => @kawasan.area_id)
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    if params[:jenis_layanan_id] && params[:jenis_layanan_id] != ""
      @users = User.where("id IN (?) AND jenis_layanan_id = ?", @users.map(&:id), params[:jenis_layanan_id].to_i)
    end

    if params[:area_id] && params[:area_id] != ""
      @users = User.where("id IN (?) AND area_new_id = ?", @users.map(&:id), params[:area_id])
    end

    if params[:kawasan_id] && params[:kawasan_id] != ""
      @users = User.where("id IN (?) AND kawasan_id = ?", @users.map(&:id), params[:kawasan_id].to_i)
    end

    if params[:tarip] && params[:tarip] != ""
      @users = User.where("id IN (?) AND tarip = ?", @users.map(&:id), params[:tarip])
    end

    if params[:daya_awal] && params[:daya_awal] != ""
      @daya_awal = params[:daya_awal].to_i
    else
      @daya_awal = 0
    end

    if params[:daya_akhir] && params[:daya_akhir] != ""
      @daya_akhir = params[:daya_akhir].to_i
    else
      @daya_akhir = 99999999999999999
    end

    @users = User.where("id IN (?) AND daya >= ? AND daya <= ?", @users.map(&:id), @daya_awal, @daya_akhir)

	filename = "Data Pelanggan Potensial.xls" 
	
    respond_to do |format|
      format.html # index.html.erb
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
      format.json { render json: @users }
    end
  end

  def trash
    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id != 1")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND flag_status = 0", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND flag_status = 0", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
        @areas = Area.where(:id => @kawasan.area_id)
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND flag_status = 0", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND flag_status = 0", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    if params[:jenis_layanan_id] && params[:jenis_layanan_id] != ""
      @users = User.where("id IN (?) AND jenis_layanan_id = ?", @users.map(&:id), params[:jenis_layanan_id].to_i)
    end

    if params[:area_id] && params[:area_id] != ""
      @users = User.where("id IN (?) AND area_new_id = ?", @users.map(&:id), params[:area_id])
    end

    if params[:kawasan_id] && params[:kawasan_id] != ""
      @users = User.where("id IN (?) AND kawasan_id = ?", @users.map(&:id), params[:kawasan_id].to_i)
    end

    if params[:tarip] && params[:tarip] != ""
      @users = User.where("id IN (?) AND tarip = ?", @users.map(&:id), params[:tarip])
    end

    if params[:daya_awal] && params[:daya_awal] != ""
      @daya_awal = params[:daya_awal].to_i
    else
      @daya_awal = 0
    end

    if params[:daya_akhir] && params[:daya_akhir] != ""
      @daya_akhir = params[:daya_akhir].to_i
    else
      @daya_akhir = 99999999999999999
    end

    @users = User.where("id IN (?) AND daya >= ? AND daya <= ?", @users.map(&:id), @daya_awal, @daya_akhir)

  filename = "Data Pelanggan Potensial.xls" 
  
    respond_to do |format|
      format.html # index.html.erb
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")
    @dayas = Daya.order("name DESC")
    @master_tarifs = MasterTarif.all

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")
    @dayas = Daya.order("name DESC")
    @master_tarifs = MasterTarif.all

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")
    @dayas = Daya.order("name DESC")
    @master_tarifs = MasterTarif.all
    if Daya.find(params[:user][:daya_id])
      @user.daya = Daya.find(params[:user][:daya_id]).name.to_i
    else
      @user.daya = 0
    end

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end

    respond_to do |format|
      if @user.save
        if @user.area
          @user.update_attributes(:area_id => @user.area.id)
        end
        
        if @user.jenis_layanan && @user.jenis_layanan.tipe_jenis_layanan_id == 1
          format.html { redirect_to admin_premium_user_path(@user), notice: 'User was successfully created.' }
          format.json { render json: @user, status: :created, location: @user }
        else
          format.html { redirect_to [:admin,@user], notice: 'User was successfully created.' }
          format.json { render json: @user, status: :created, location: @user }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")
    @dayas = Daya.order("name DESC")
    @master_tarifs = MasterTarif.all
    if Daya.find(params[:user][:daya_id])
      @user.daya = Daya.find(params[:user][:daya_id]).name.to_i
    else
      @user.daya = 0
    end

    if current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        if @kawasan.area
          @rayons = @kawasan.area.rayons.order("name ASC")
        end
        @areas = Area.where(:id => @kawasan.area_id)
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)

      if @rayon
        @areas = Area.where(:area_id => @rayon.area_new_id)
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
      end
    end

    respond_to do |format|
      if @user.update_attributes(user_params)
        if @user.area
          @user.update_attributes(:area_id => @user.area.id)
        end

        if @user.jenis_layanan.tipe_jenis_layanan_id == 1
          format.html { redirect_to admin_premium_user_path(@user), notice: 'User was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { redirect_to [:admin,@user], notice: 'User was successfully updated.' }
          format.json { head :no_content }
        end
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.update_attributes(:flag_status => 0)

    respond_to do |format|
      format.html { redirect_to admin_users_url }
      format.json { head :no_content }
    end
  end

  def restore
    @user = User.find(params[:id])
    @user.update_attributes(:flag_status => 1)

    respond_to do |format|
      format.html { redirect_to admin_users_url }
      format.json { head :no_content }
    end
  end

  def send_to_trash
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  def reset_password
    @user = User.find(params[:id])
    @user.reset_password!(@user.username+"12345",@user.username+"12345")
    
    redirect_to :back
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :remember_me, 
  :username, :user_group_id, :rayon_id, :area_new_id, :rayon_new_id, :area_id,
  :idpel, :name, :alamat_pelanggan, :nomor_bangunan, :keterangan_bangunan, :nomor_rt, :nomor_rw, 
  :lingkungan, :nomor_dalam_rt, :desa_kelurahan, :subdistrict_id, :district_id, :rupiah_ujl, :no_kuit_ujl,
  :province_id, :kode_pos, :nomor_telepon, :nomor_fax, :tarip, :kdaya, :jenis_layanan_id, :tanggal_layanan_reguler,
  :nomor_pks, :tanggal_pks, :file_pks, :merk_meter_id, :type_meter_id, :nomor_meter, :tgl_kuit_ujl_date,
  :kosntanta_meter, :trafo_arus_primer_kwh, :trafo_arus_sekunder_kwh, :trafo_tegangan_primer_kwh, :perkiraan_biaya_investasi,
  :trafo_tegangan_sekunder_kwh, :faktor_kali_meter, :tanggal_pasang_baru, :tanggal_layanan_premium, :kawasan_id, :daya_id,
  :pasokan_gi_1, :pasokan_penyulang_1, :pasokan_gi_2, :pasokan_penyulang_2, :pasokan_gi_3, :tegangan, :nomor_dlm_rt,
  :pasokan_penyulang_3, :pasokan_gi_4, :pasokan_penyulang_4, :nomor_gardu, :nama_gardu, :kapasitas_trafo, :jenis_tipe_gardu, 
  :jenis_usaha, :nomor_klui, :jenis_bahan_baku, :jenis_komoditi_produk, :kapasitas_produksi_per_tahun, :master_tarif_id,
  :jumlah_tenaga_kerja, :mesin_peralatan_utama, :mesin_peralatan_pembantu, :nama_dirut_string, :nama_wakil_dirut, :flag_status,
  :nama_contact, :no_telepon_contact, :nomor_hp_contact, :nama_contact_2, :no_telp_contact_2, :no_hp_contact_2, 
  :email_contact, :email_contact_2, :jabatan_contact, :jabatan_contact_2, :no_fax_contact, :koordinat_x, :koordinat_y, :avatar,
  :kapasitas_trafo_induk_1, :nomor_trafo_induk_1, :kapasitas_trafo_induk_2, :nomor_trafo_induk_2, :kapasitas_trafo_induk_3, :nomor_trafo_induk_3,
  :kapasitas_trafo_induk_4, :nomor_trafo_induk_4, :nomor_adendum_spjbtl, :is_active, :daya, :tanggal_addendum_spjbtl, :file_pks, :avatar)
  end
end
