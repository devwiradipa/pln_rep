class Admin::SlideshowsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @slideshows = Slideshow.order("created_at DESC")
  end
  
  def new
    @slideshow = Slideshow.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @slideshow }
    end
  end
  
  def show
    @slideshow = Slideshow.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @slideshow }
    end
  end
  
  def create
    @slideshow = Slideshow.new(slideshow_params)
    @slideshow.featured_status = 0

    respond_to do |format|
      if @slideshow.save
        
        format.html { redirect_to(admin_slideshow_path(@slideshow), :notice => 'slideshow was successfully created.') }
        format.xml  { render :xml => @slideshow, :status => :created, :slideshow => @slideshow }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @slideshow.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @slideshow = Slideshow.find(params[:id])
  end
  
  def update
    @slideshow = Slideshow.find(params[:id])

    respond_to do |format|
      if @slideshow.update_attributes(slideshow_params)
        
        format.html { redirect_to(admin_slideshow_path(@slideshow), :notice => 'slideshow was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @slideshow.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @slideshow = Slideshow.find(params[:id])
    @slideshow.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/slideshows') }
      format.xml  { head :ok }
    end
  end

  def featured
    @slideshow = Slideshow.find(params[:id])
    if @slideshow.featured_status == 1
      @slideshow.update_attributes(:featured_status => 0)
    else
      @slideshow.update_attributes(:featured_status => 1)
    end

    redirect_to :back
  end

  private

  def slideshow_params
    params.require(:slideshow).permit(:title, :image, :featured_status)
  end
end
