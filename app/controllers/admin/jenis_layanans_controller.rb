class Admin::JenisLayanansController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @jenis_layanans = JenisLayanan.all
  end
  
  def new
    @jenis_layanan = JenisLayanan.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @jenis_layanan }
    end
  end
  
  def show
    @jenis_layanan = JenisLayanan.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @jenis_layanan }
    end
  end
  
  def create
    @jenis_layanan = JenisLayanan.new(jenis_layanan_params)

    respond_to do |format|
      if @jenis_layanan.save
        
        format.html { redirect_to(admin_jenis_layanans_path, :notice => 'jenis_layanan was successfully created.') }
        format.xml  { render :xml => @jenis_layanan, :status => :created, :jenis_layanan => @jenis_layanan }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @jenis_layanan.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @jenis_layanan = JenisLayanan.find(params[:id])
  end
  
  def update
    @jenis_layanan = JenisLayanan.find(params[:id])

    respond_to do |format|
      if @jenis_layanan.update_attributes(jenis_layanan_params)
        
        format.html { redirect_to(admin_jenis_layanans_path, :notice => 'jenis_layanan was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @jenis_layanan.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @jenis_layanan = JenisLayanan.find(params[:id])
    @jenis_layanan.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/jenis_layanans') }
      format.xml  { head :ok }
    end
  end

  private

  def jenis_layanan_params
    params.require(:jenis_layanan).permit(:created_by, :description, :name, :tarif, :updated_by, :tipe_jenis_layanan_id)
  end
end
