class Admin::AreasController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @areas = Area.order("name DESC")
  end
  
  def new
    @area = Area.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @area }
    end
  end
  
  def show
    @area = Area.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @area }
    end
  end
  
  def create
    @area = Area.new(area_params)

    respond_to do |format|
      if @area.save
        
        format.html { redirect_to(admin_areas_path, :notice => 'area was successfully created.') }
        format.xml  { render :xml => @area, :status => :created, :area => @area }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @area.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @area = Area.find(params[:id])
  end
  
  def update
    @area = Area.find(params[:id])

    respond_to do |format|
      if @area.update_attributes(area_params)
        
        format.html { redirect_to(admin_areas_path, :notice => 'area was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @area.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @area = Area.find(params[:id])
    @area.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/areas') }
      format.xml  { head :ok }
    end
  end

  private

  def area_params
    params.require(:area).permit(:created_by, :name, :updated_by, :telp, :alamat, :fax, :area_new_id,
  :pic_name_1, :pic_jabatan_1, :pic_hp_1, :pic_name_2, :pic_jabatan_2, :pic_hp_2, 
  :pic_name_3, :pic_jabatan_3, :pic_hp_3, :pic_name_4, :pic_jabatan_4, :pic_hp_4, :area_id)
  end
end
