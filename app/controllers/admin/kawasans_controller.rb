class Admin::KawasansController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @kawasans = Kawasan.order("nama_kawasan DESC")
  end
  
  def new
    @kawasan = Kawasan.new
    @areas = Area.order("name ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @kawasan }
    end
  end
  
  def show
    @kawasan = Kawasan.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @kawasan }
    end
  end
  
  def create
    @kawasan = Kawasan.new(kawasan_params)
    @areas = Area.order("name ASC")

    respond_to do |format|
      if @kawasan.save
        
        format.html { redirect_to(admin_kawasans_path, :notice => 'kawasan was successfully created.') }
        format.xml  { render :xml => @kawasan, :status => :created, :kawasan => @kawasan }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @kawasan.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @kawasan = Kawasan.find(params[:id])
    @areas = Area.order("name ASC")
  end
  
  def update
    @kawasan = Kawasan.find(params[:id])
    @areas = Area.order("name ASC")

    respond_to do |format|
      if @kawasan.update_attributes(kawasan_params)
        
        format.html { redirect_to(admin_kawasans_path, :notice => 'kawasan was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @kawasan.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @kawasan = Kawasan.find(params[:id])
    @kawasan.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/kawasans') }
      format.xml  { head :ok }
    end
  end

  private

  def kawasan_params
    params.require(:kawasan).permit(:created_by, :kawasan_id, :nama_kawasan, :updated_by, :description, :area_id, :telp, :alamat, :fax,
  :pic_name_1, :pic_jabatan_1, :pic_hp_1, :pic_name_2, :pic_jabatan_2, :pic_hp_2, 
  :pic_name_3, :pic_jabatan_3, :pic_hp_3, :pic_name_4, :pic_jabatan_4, :pic_hp_4)
  end
end
