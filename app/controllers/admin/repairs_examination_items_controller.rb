class Admin::RepairsExaminationItemsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @repairs_examination_items = RepairsExaminationItem.order("name DESC")
  end
  
  def new
    @repairs_examination_item = RepairsExaminationItem.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @repairs_examination_item }
    end
  end
  
  def show
    @repairs_examination_item = RepairsExaminationItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @repairs_examination_item }
    end
  end
  
  def create
    @repairs_examination_item = RepairsExaminationItem.new(repair_examination_item_params)

    respond_to do |format|
      if @repairs_examination_item.save
        
        format.html { redirect_to(admin_repairs_examination_items_path, :notice => 'repairs_examination_item was successfully created.') }
        format.xml  { render :xml => @repairs_examination_item, :status => :created, :repairs_examination_item => @repairs_examination_item }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @repairs_examination_item.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @repairs_examination_item = RepairsExaminationItem.find(params[:id])
    @officers = Officer.all
  end
  
  def update
    @repairs_examination_item = RepairsExaminationItem.find(params[:id])
    @officers = Officer.all

    if params[:datetime_time]
      params[:repairs_examination_item][:repair_time] = params[:repairs_examination_item][:repair_time] + " " + params[:datetime_time]
    end

    respond_to do |format|
      if @repairs_examination_item.update_attributes(repair_examination_item_params)
        
        format.html { redirect_to(admin_repair_path(@repairs_examination_item.repair), :notice => 'repairs_examination_item was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @repairs_examination_item.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @repairs_examination_item = RepairsExaminationItem.find(params[:id])
    @repairs_examination_item.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/repairs_examination_items') }
      format.xml  { head :ok }
    end
  end

  private

  def repair_examination_item_params
    params.require(:repair_examination_item).permit(:examination_item_id, :note, :officer_id, :repair_id, :repair_time)
  end
end
