class Admin::ExaminationsController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :authorize_admin
    
  layout "admin"
  
  def index
    @jenis_layanans = JenisLayanan.all
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
      @areas = Area.order("name ASC")
      @kawasans = Kawasan.order("nama_kawasan ASC")
      @rayons = Rayon.order("name ASC")
      @examinations = Examination.order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      @kawasans = Kawasan.where(:id => current_admin.kawasan_id)
      if @kawasan
        @areas = Area.where("id IS NULL")
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
        @rayons = Rayon.where("id IS NULL")
        @examinations = Examination.where("user_id IN (?)", @users.map(&:id)).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
        @areas = Area.where("id IS NULL")
        @rayons = Rayon.where("id IS NULL")
        @kawasans = Kawasan.where("id IS NULL")
        @examinations = Examination.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      @rayons = Rayon.where(:rayon_id => current_admin.rayon_new_id)
      if @rayon
        @kawasans = @rayon.area.kawasans.order("nama_kawasan ASC")
        @areas = Area.where("area_id = ?", @rayon.area_new_id)
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
        @examinations = Examination.where("user_id IN (?)", @users.map(&:id)).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
        @areas = Area.where("id IS NULL")
        @rayons = Rayon.where("id IS NULL")
        @kawasans = Kawasan.where("id IS NULL")
        @examinations = Examination.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      @areas = Area.where(:area_id => current_admin.area_new_id)
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
        @kawasans = @area.kawasans.order("nama_kawasan ASC")
        @rayons = @area.rayons.order("name ASC")
        @examinations = Examination.where("user_id IN (?)", @users.map(&:id)).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
        @areas = Area.where("id IS NULL")
        @rayons = Rayon.where("id IS NULL")
        @kawasans = Kawasan.where("id IS NULL")
        @examinations = Examination.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL") 
      @areas = Area.where("id IS NULL")
      @rayons = Rayon.where("id IS NULL")
      @kawasans = Kawasan.where("id IS NULL")
      @examinations = Examination.where("id IS NULL")
    end

    @examination_indicator = ExaminationIndicator.where("status = 1").order("updated_at DESC").first
  end
  
  def new
    @examination_indicator = ExaminationIndicator.where("status = 1").order("updated_at DESC").first
    if !@examination_indicator
      redirect_to admin_examinations_path
    else
       @examination = Examination.new
	     
	     @officers = Officer.order("name ASC")
	     @examination_indicator.examination_items.where("parent_id IS NULL").each do |examination_item|
         @examination.examination_examination_items.build(:examination_item_id => examination_item.id, :condition => 0)
         examination_item.children.each do |child|
            @examination.examination_examination_items.build(:examination_item_id => child.id, :condition => 0)
            child.children.each do |item|
              @examination.examination_examination_items.build(:examination_item_id => item.id, :condition => 0)
           end
         end
       end

       @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")

       if current_admin.user_group && current_admin.user_group.super == 1
          @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
        elsif current_admin.user_group && current_admin.kawasan
          @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
          if @kawasan
            @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
          else
            @users = User.where("id IS NULL") 
          end
        elsif current_admin.user_group && current_admin.rayon
          @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
          if @rayon
            @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
          else
            @users = User.where("id IS NULL")
          end
        elsif current_admin.user_group && current_admin.area
          @area = Area.where(:area_id => current_admin.area_new_id).first
          if @area
            @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
          else
            @users = User.where("id IS NULL")
          end
        else
          @users = User.where("id IS NULL")
        end
		
  	   respond_to do |format|
      		format.html # new.html.erb
      		format.xml  { render :xml => @examination }
       end
    end
  end
  
  def show
    @examination = Examination.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.pdf  { render :pdf => "Pemeriksaan", :page_size => 'A4' }
      format.xml  { render :xml => @examination }
    end
  end
  
  def create
    @examination = Examination.new(examination_params)
    @examination_indicator = ExaminationIndicator.where("status = 1").order("updated_at DESC").first

    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    @officers = Officer.order("name ASC")
    if params[:datetime_time]
      @examination.examination_time = @examination.examination_time.strftime("%Y-%m-%d") + " " + params[:datetime_time]
    end

    respond_to do |format|
      if @examination.save
      
        examination_examination_items = ExaminationExaminationItem.where(:examination_id => @examination.id , :condition => 3)
        if examination_examination_items.count > 0
          @repair = Repair.new(:user_id => @examination.user_id, :examination_id => @examination.id)
          @repair.save
          examination_examination_items.each do |examination_examination_item|
            @repairs_examination_item = RepairsExaminationItem.new(:repair_id => @repair.id, :examination_item_id => examination_examination_item.examination_item_id)
            @repairs_examination_item.save
          end
        end
        
        format.html { redirect_to(admin_examination_path(@examination), :notice => 'examination was successfully created.') }
        format.xml  { render :xml => @examination, :status => :created, :examination => @examination }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @examination.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @examination = Examination.find(params[:id])
    @examination_indicator = @examination.examination_indicator

    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    @officers = Officer.order("name ASC")
  end
  
  def update
    @examination = Examination.find(params[:id])
    @examination_indicator = @examination.examination_indicator

    @jenis_layanans = JenisLayanan.where("tipe_jenis_layanan_id = 1")
    
    if current_admin.user_group && current_admin.user_group.super == 1
      @users = User.where("jenis_layanan_id IN (?) AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id)).order("created_at DESC")
    elsif current_admin.user_group && current_admin.kawasan
      @kawasan = Kawasan.where(:id => current_admin.kawasan_id).first
      if @kawasan
        @users = User.where("jenis_layanan_id IN (?) AND kawasan_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @kawasan.id).order("created_at DESC")
      else
        @users = User.where("id IS NULL") 
      end
    elsif current_admin.user_group && current_admin.rayon
      @rayon = Rayon.where(:rayon_id => current_admin.rayon_new_id).first
      if @rayon
        @users = User.where("jenis_layanan_id IN (?) AND rayon_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @rayon.rayon_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    elsif current_admin.user_group && current_admin.area
      @area = Area.where(:area_id => current_admin.area_new_id).first
      if @area
        @users = User.where("jenis_layanan_id IN (?) AND area_new_id = ? AND (flag_status IS NULL OR flag_status = 1)", @jenis_layanans.map(&:id), @area.area_id).order("created_at DESC")
      else
        @users = User.where("id IS NULL")
      end
    else
      @users = User.where("id IS NULL")
    end

    @officers = Officer.order("name ASC")
    if params[:datetime_time]
      params[:examination][:examination_time] = params[:examination][:examination_time] + " " + params[:datetime_time]
    end

    respond_to do |format|
      if @examination.update_attributes(examination_params)
        format.html { redirect_to(admin_examination_path(@examination), :notice => 'examination was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @examination.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @examination = Examination.find(params[:id])
    @examination.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'admin/examinations') }
      format.xml  { head :ok }
    end
  end

  private

  def examination_params
    params.require(:examination).permit(:created_by, :examination_indicator_id, :examination_time, :shift, :updated_by, :user_id, :officer_ids, :examination_item_ids,
  examination_examination_items_attributes: [:condition, :examination_id, :examination_item_id, :note, :_destroy])
  end
end
