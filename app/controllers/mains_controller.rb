class MainsController < ApplicationController
	layout "application_home"

	def index
		@articles = Article.where("featured_status = 1").order("created_at ASC").limit(2)
		@testimonials = Testimonial.where("featured = 1")
		@slideshows = Slideshow.where("featured_status = 1").order("created_at ASC")
		@what = BodyContent.where("title = 'what'").first
	end
end
