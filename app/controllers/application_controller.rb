class ApplicationController < ActionController::Base
  protect_from_forgery
  
  layout :layout_by_resource
  
  before_filter :controller_model
  
  protected

  def controller_model
    @months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Jul", "Agustus", "September", "Oktober", "November", "Desember"]
    @days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
    @conditions = ["N/A", "Baik", "Tidak Baik", "Tidak Berfungsi"]
    @now = Time.now
    @hostname = "http://pcms.pln-jabar.co.id"

    if current_admin
      if current_admin.user_group && current_admin.user_group.super == 1
        @unread_feedbacks = DataFeedback.where("parent_id IS NULL AND `read` IS NULL")
      elsif current_admin.user_group && current_admin.rayon
        if Rayon.where("rayon_id = ?", current_admin.rayon_new_id).first
          rayon = Rayon.where("rayon_id = ?", current_admin.rayon_new_id).first
          @users = User.where("rayon_new_id = ?", rayon.rayon_id)
          @unread_feedbacks = DataFeedback.where("parent_id IS NULL AND user_id IN (?) AND `read` IS NULL", @users.map(&:id))
        else
          @unread_feedbacks = DataFeedback.where("id IS NULL")
        end
      elsif current_admin.user_group && current_admin.area
        if Area.where("area_id = ?", current_admin.area_new_id).first
          area = Area.where("area_id = ?", current_admin.area_new_id).first
          @users = User.where("area_new_id = ?", area.area_id)
          @unread_feedbacks = DataFeedback.where("parent_id IS NULL AND user_id IN (?) AND `read` IS NULL", @users.map(&:id))
        else
          @unread_feedbacks = DataFeedback.where("id IS NULL")
        end
      else
        @unread_feedbacks = DataFeedback.where("id IS NULL")
      end
    end
  end
  
  def layout_by_resource
    if devise_controller?
      if resource_name == :admin
        "admin_login"
      else
        "login"
      end
    else
      "application"
    end
  end
  
  def after_sign_in_path_for(resource_or_scope)
    if resource_or_scope.is_a?(Admin)
      admin_root_path
    elsif resource_or_scope.is_a?(User)
      homes_path
    else
      super
    end
  end
  
  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope.to_s == "admin"
      new_admin_session_path
    else
      root_path
    end
  end
  
  def authorize_user
    @not_authorized = true
    current_user.user_group.action_models.each do |action_model|
      if params[:controller] == action_model.controller_model.path && params[:action] == action_model.path
        @not_authorized = false
      end
      
      break if ! @not_authorized
    end
    
    @action_model = ActionModel.where("path = ?", params[:action]).first
    
    if ! @action_model
      @not_authorized = false
    end
    
    if current_user.username == "admin"
      @not_authorized = false
    end
    
    if @not_authorized
      redirect_to(:controller => "homes")
    end
  end

  def authorize_admin
    @not_authorized = true
    current_admin.user_group.action_models.each do |action_model|
      if params[:controller] == action_model.controller_model.path && params[:action] == action_model.path
        @not_authorized = false
      end
      
      break if ! @not_authorized
    end
    
    @action_model = ActionModel.where("path = ?", params[:action]).first
    
    if ! @action_model
      @not_authorized = false
    end
    
    if current_admin.user_group && current_admin.user_group.super == 1
      @not_authorized = false
    end
    
    if @not_authorized
      redirect_to(:controller => "/admins")
    end
  end
end
