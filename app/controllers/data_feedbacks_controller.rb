class DataFeedbacksController < ApplicationController
	before_filter :authenticate_user!

	def index
		@data_feedbacks = DataFeedback.where("parent_id IS NULL AND user_id = ?", current_user).order("created_at DESC")
	end
  
  def new
    @data_feedback = DataFeedback.new
    @feedback_types = FeedbackType.all
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_feedback }
    end
  end
  
  def show
    @data_feedback = DataFeedback.find(params[:id])
    @data_feedback.update_attributes(user_read: 1, user_read_by: current_user.id, user_read_at: Time.now)
    @data_feedback.replies.each do |reply|
      reply.update_attributes(user_read: 1, user_read_by: current_user.id, user_read_at: Time.now)
    end
    
    @new_data_feedback = DataFeedback.new

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_feedback }
    end
  end
  
  def create
    @data_feedback = DataFeedback.new(data_feedback_params)
    @feedback_types = FeedbackType.all
    @data_feedback.user_read = 1
    @data_feedback.user_read_by = current_user.id
    @data_feedback.user_read_at = Time.now

    respond_to do |format|
      if @data_feedback.save

        if @data_feedback.user.kawasan
          @data_feedback.user.kawasan.admins.each do |admin|
            if admin.email
              # UserMailer.feedback_notification(@data_feedback,admin).deliver
              # sidekiq can only use non object parameter
              feedback_plg = @data_feedback.feedback_plg
              created_by = @data_feedback.user.name
              receiver_name = admin.name
              if @data_feedback.parent
                id = @data_feedback.parent_id.to_s
              else
                id = @data_feedback.id.to_s
              end
              email = admin.email
              UserMailer.feedback_notification(feedback_plg, created_by, receiver_name, id, email, @hostname).deliver
            end
          end
        end

        if @data_feedback.user.rayon
          @data_feedback.user.rayon.admins.each do |admin|
            if admin.email
              # UserMailer.feedback_notification(@data_feedback,admin).deliver
              # sidekiq can only use non object parameter
              feedback_plg = @data_feedback.feedback_plg
              created_by = @data_feedback.user.name
              receiver_name = admin.name
              if @data_feedback.parent
                id = @data_feedback.parent_id.to_s
              else
                id = @data_feedback.id.to_s
              end
              email = admin.email
              UserMailer.feedback_notification(feedback_plg, created_by, receiver_name, id, email, @hostname).deliver
            end
          end
        end

        if @data_feedback.user.area
          @data_feedback.user.area.admins.each do |admin|
            if admin.email
              # UserMailer.feedback_notification(@data_feedback,admin).deliver
              # sidekiq can only use non object parameter
              feedback_plg = @data_feedback.feedback_plg
              created_by = @data_feedback.user.name
              receiver_name = admin.name
              if @data_feedback.parent
                id = @data_feedback.parent_id.to_s
              else
                id = @data_feedback.id.to_s
              end
              email = admin.email
              UserMailer.feedback_notification(feedback_plg, created_by, receiver_name, id, email, @hostname).deliver
            end
          end
        end

        @super = UserGroup.where("super = 1")
        @admin_kds = Admin.where("user_group_id IN (?)", @super.map(&:id))

        @admin_kds.each do |admin|
          if admin.email
            feedback_plg = @data_feedback.feedback_plg
            created_by = @data_feedback.user.name
            receiver_name = admin.name
            if @data_feedback.parent
              id = @data_feedback.parent_id.to_s
            else
              id = @data_feedback.id.to_s
            end
            email = admin.email
            UserMailer.feedback_notification(feedback_plg, created_by, receiver_name, id, email, @hostname).deliver
          end
        end

        if @data_feedback.parent
          @data_feedback.parent.update_attributes(:status_feedback => "belum dibalas")

        	format.html { redirect_to(data_feedback_path(@data_feedback.parent), :notice => 'data_feedback was successfully created.') }
	        format.xml  { render :xml => @data_feedback, :status => :created, :data_feedback => @data_feedback }
        else
	        format.html { redirect_to(data_feedbacks_path, :notice => 'data_feedback was successfully created.') }
	        format.xml  { render :xml => @data_feedback, :status => :created, :data_feedback => @data_feedback }
	    end
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_feedback.errors, :status => :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def data_feedback_params
      params.require(:data_feedback).permit(:feedback_plg, :feedback_type_id, :user_id, :status_feedback, :featured, :created_by, :updated_by)
    end
end
