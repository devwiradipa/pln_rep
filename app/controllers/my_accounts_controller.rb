class MyAccountsController < ApplicationController
	before_filter :authenticate_user!
	  
	def index
	   	@user = current_user
	end

  # GET /users/1/edit
  def edit
    @my_account = User.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @my_account = User.find(params[:id])
    @user_groups = UserGroup.order("name ASC")
    @rayons = Rayon.order("name ASC")
    @areas = Area.order("name ASC")
    @kawasans = Kawasan.order("nama_kawasan ASC")
    @provinces = Province.all
    @districts = District.all
    @subdistricts = Subdistrict.all
    @jenis_layanans = JenisLayanan.all
    @merk_meters = MerkMeter.order("name ASC")
    @type_meters = TypeMeter.order("name ASC")

    respond_to do |format|
      if @my_account.update_attributes(params[:my_account])
        format.html { redirect_to @my_accounts_path, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
