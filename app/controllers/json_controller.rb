class JsonController < ApplicationController
	def get_district
	    @districts = District.where(:province_id => params[:province_id])

	    respond_to do |format|
	      format.js {
	        render :json => @districts.to_json(:except => [:updated_at]),
	        :callback => params[:callback]
	      }
	    end
	end

	def get_subdistrict
	    @subdistricts = Subdistrict.where(:district_id => params[:district_id])

	    respond_to do |format|
	      format.js {
	        render :json => @subdistricts.to_json(:except => [:updated_at]),
	        :callback => params[:callback]
	      }
	    end
	end
end
