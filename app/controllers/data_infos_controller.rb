class DataInfosController < ApplicationController
	before_filter :authenticate_user!

	def index
    if current_user.kawasan && current_user.area
		  @data_infos = DataInfo.where("id IN (?) || id IN (?) || id IN (?)", current_user.data_infos.map(&:id), current_user.kawasan.data_infos.map(&:id), current_user.area.data_infos.map(&:id)).order("tgl_info_gangguan DESC")
    elsif current_user.kawasan
      @data_infos = DataInfo.where("id IN (?) || id IN (?)", current_user.data_infos.map(&:id), current_user.kawasan.data_infos.map(&:id)).order("tgl_info_gangguan DESC")
    elsif current_user.area
      @data_infos = DataInfo.where("id IN (?) || id IN (?)", current_user.data_infos.map(&:id), current_user.area.data_infos.map(&:id)).order("tgl_info_gangguan DESC")
    else
      @data_infos = current_user.data_infos.order("tgl_info_gangguan DESC")
    end
	end
  
  def show
    @data_info = DataInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_info }
    end
  end
end
