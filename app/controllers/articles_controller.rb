class ArticlesController < ApplicationController
	layout "article"

	def index
		@articles = Article.order("created_at DESC")
	end

	def show
		@article = Article.find(params[:id])
		@articles = Article.order("created_at DESC").limit(3)
	end
end
