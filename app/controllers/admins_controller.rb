class AdminsController < ApplicationController
  before_filter :authenticate_admin!

  layout "admin"

  def index

  end

  def edit_password
    @admin = current_admin
  end

  # PUT /admins/1
  # PUT /admins/1.json
  def update_password
    @admin = Admin.find(params[:id])

    respond_to do |format|
      if @admin.update_with_password(params[:admin])
        format.html { redirect_to [:admin, :my_accounts], notice: 'Admin was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit_password" }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end
end
