class HomesController < ApplicationController
	before_filter :authenticate_user!

	def index
		@monthly_info = current_user.monthly_infos.order("created_at DESC").first
	end

	def get_data
		if params[:blth_bulan_awal] && params[:blth_bulan_awal].to_i != 0
			@blth_bulan_awal = params[:blth_bulan_awal].to_i
		else
			@blth_bulan_awal = Time.now.month-2
		end

		if params[:blth_bulan_akhir] && params[:blth_bulan_akhir].to_i != 0
			@blth_bulan_akhir = params[:blth_bulan_akhir].to_i
		else
			@blth_bulan_akhir = Time.now.month
		end

		if params[:blth_tahun_awal] && params[:blth_tahun_awal].to_i != 0
			@blth_tahun_awal = params[:blth_tahun_awal].to_i
		else
			@blth_tahun_awal = Time.now.year
		end

		if params[:blth_tahun_akhir] && params[:blth_tahun_akhir].to_i != 0
			@blth_tahun_akhir = params[:blth_tahun_akhir].to_i
		else
			@blth_tahun_akhir = Time.now.year
		end

		if @blth_bulan_awal < 0
			@blth_bulan_awal = @blth_bulan_awal+12
			@blth_tahun_awal = @blth_tahun_awal-1
		end

		@monthly_infos = MonthlyInfo.select("monthly_infos.durasi_gangguan, monthly_infos.frekuensi_gangguan, 
           monthly_infos.bulan_blth as bulan_blth, monthly_infos.tahun_blth as tahun_blth, 
           monthly_infos.kwh_kvarh as kwh_kvarh, monthly_infos.kwh_lwbp as kwh_lwbp, 
           monthly_infos.kwh_wbp as kwh_wbp, monthly_infos.kwh_total as kwh_total,  
           monthly_infos.rp_kvarh as rp_kvarh, monthly_infos.rp_lwbp as rp_lwbp,  
           monthly_infos.rp_wbp as rp_wbp, monthly_infos.rptag as rptag").
           where("((monthly_infos.bulan_blth >= ? AND monthly_infos.tahun_blth >= ?) OR (monthly_infos.bulan_blth < ? AND monthly_infos.tahun_blth > ?)) AND ((monthly_infos.bulan_blth <= ? AND monthly_infos.tahun_blth <= ?) OR (monthly_infos.bulan_blth > ? AND monthly_infos.tahun_blth < ?)) AND user_id = ?", @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_awal, @blth_tahun_awal, @blth_bulan_akhir, @blth_tahun_akhir, @blth_bulan_akhir, @blth_tahun_akhir, current_user.id).
           order("monthly_infos.tahun_blth ASC, monthly_infos.bulan_blth ASC")

		json_data = Hash.new

		blth = []
		blth_pemakaian = []
		durasi_gangguan = []
		frekuensi_gangguan = []
		kwh_kvarh = []
		kwh_lwbp = []
		kwh_wbp = []
		kwh_total = []
		rp_kvarh = []
		rp_lwbp = []
		rp_wbp = []
		rp_total = []

		@monthly_infos.each do |monthly_info|
			if monthly_info.bulan_blth - 1 == 0
				blth_pemakaian << @months[12]+" "+(monthly_info.tahun_blth-1).to_s
			else
				blth_pemakaian << @months[monthly_info.bulan_blth-1]+" "+monthly_info.tahun_blth.to_s
			end
			blth << @months[monthly_info.bulan_blth]+" "+monthly_info.tahun_blth.to_s
			durasi_gangguan << monthly_info.durasi_gangguan
			frekuensi_gangguan << monthly_info.frekuensi_gangguan
			kwh_kvarh << monthly_info.kwh_kvarh
			kwh_lwbp << monthly_info.kwh_lwbp
			kwh_wbp << monthly_info.kwh_wbp
			kwh_total << monthly_info.kwh_total
			rp_kvarh << monthly_info.rp_kvarh
			rp_lwbp << monthly_info.rp_lwbp
			rp_wbp << monthly_info.rp_wbp
			rp_total << monthly_info.rptag
		end

		json_data[:blth_pemakaian] = blth_pemakaian
		json_data[:blth] = blth
		json_data[:durasi_gangguan] = durasi_gangguan
		json_data[:frekuensi_gangguan] = frekuensi_gangguan
		json_data[:kwh_kvarh] = kwh_kvarh
		json_data[:kwh_lwbp] = kwh_lwbp
		json_data[:kwh_wbp] = kwh_wbp
		json_data[:kwh_total] = kwh_total
		json_data[:rp_kvarh] = rp_kvarh
		json_data[:rp_lwbp] = rp_lwbp
		json_data[:rp_wbp] = rp_wbp
		json_data[:rp_total] = rp_total

		render :json => json_data, :layout => false
	end
end
