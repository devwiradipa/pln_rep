class UserMailer < ActionMailer::Base
  include Sidekiq::Mailer
  default from: "PLN DJBB <info.pcms@pln.co.id>"
  
  # def feedback_notification(feedback, receiver)
  #   @body = feedback.feedback_plg
  #   @created_by = feedback.user.name
  #   @receiver_name = receiver.name
  #   if feedback.parent
  #   	@url  = "http://crm_pln.wiradipa.com/data_feedbacks/"+feedback.parent_id.to_s
  #   else
  #   	@url  = "http://crm_pln.wiradipa.com/data_feedbacks/"+feedback.id.to_s
  #   end
  #   mail(:to => receiver.email, :subject => "Feedback From "+@created_by)
  # end

  # def reply_notification(feedback)
  #   @receiver_name = feedback.parent.user.name

  #   @url  = "http://crm_pln.wiradipa.com/data_feedbacks/"+feedback.parent_id.to_s

  #   mail(:to => feedback.parent.user.email, :subject => "RE: Feedback From "+@receiver_name)
  # end

  # def data_info_notification(data_info, receiver)
  #   @body = data_info.info_gangguan
  #   @receiver_name = receiver.name
  #   @tgl_info_gangguan = data_info.tgl_info_gangguan
  #   @url  = "http://crm_pln.wiradipa.com/data_infos/"+data_info.id.to_s

  #   mail(:to => receiver.email, :subject => "Info Terkini dari PLN")
  # end

  # def data_info_email_notification(data_info, receiver)
  #   @body = data_info.info_gangguan
  #   @receiver_name = receiver
  #   @tgl_info_gangguan = data_info.tgl_info_gangguan
  #   @url  = "http://crm_pln.wiradipa.com/data_infos/"+data_info.id.to_s

  #   mail(:to => receiver, :subject => "Info Terkini dari PLN")
  # end

   def feedback_notification(feedback_plg, created_by, receiver_name, id, email, hostname)
    @body = feedback_plg
    @created_by = created_by
    @receiver_name = receiver_name

    @url  = hostname+"/admin/data_feedbacks/"+id
    mail(:to => email, :subject => "Feedback From "+@created_by)
  end

  def reply_notification(created_by, feedback_plg, receiver_name, id, email, hostname)
    @body = feedback_plg
    @receiver_name = receiver_name
    @created_by = created_by
    @url  = hostname+"/data_feedbacks/"+id

    mail(:to => email, :subject => "RE: Feedback From "+@created_by)
  end

  def admin_reply_notification(user_name, created_by, feedback_plg, receiver_name, id, email, hostname)
    @body = feedback_plg
    @receiver_name = receiver_name
    @user_name = user_name
    @created_by = created_by
    @url  = hostname+"/admin/data_feedbacks/"+id
    
    mail(:to => email, :subject => "RE: Feedback (Admin) From "+@created_by)
  end

  def data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, hostname)
    @body = info_gangguan
    @created_by = created_by
    @receiver_name = receiver_name
    @tgl_info_gangguan = tgl_info_gangguan
    @url  = hostname+"/data_infos/"+id

    mail(:to => email, :subject => "Info Terkini dari PLN")
  end

  def admin_data_info_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, hostname)
    @body = info_gangguan
    @created_by = created_by
    @receiver_name = receiver_name
    @tgl_info_gangguan = tgl_info_gangguan
    @url  = hostname+"/admin/data_infos/"+id

    mail(:to => email, :subject => "Info Terkini dari PLN")
  end

  def data_info_email_notification(created_by, info_gangguan, tgl_info_gangguan, id, receiver_name, email, hostname)
    @body = info_gangguan
    @created_by = created_by
    @receiver_name = receiver_name
    @tgl_info_gangguan = tgl_info_gangguan
    @url  = hostname+"/data_infos/"+id

    mail(:to => email, :subject => "Info Terkini dari PLN")
  end
end
